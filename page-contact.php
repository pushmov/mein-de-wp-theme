<?php
/*
* Template Name: My Template
*/
get_header(); ?>
<section class="ct-box">
</section>
<section class="ct-stromtankstellen e-station-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="box box-shadow station-list" id="station_list" data-target="3" data-permalink="<?php echo $param; ?>">
					<h1 class="entry-title"><?php the_title(); ?></h1> <!-- Page Title -->
									
				    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="entry-content-page">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->

				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
				</div>
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
