<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mein-e-fahrzeug
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title hide">
			<?php
			$mein_e_fahrzeug_comment_count = get_comments_number();
			if ( '1' === $mein_e_fahrzeug_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'mein-e-fahrzeug' ),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $mein_e_fahrzeug_comment_count, 'comments title', 'mein-e-fahrzeug' ) ),
					number_format_i18n( $mein_e_fahrzeug_comment_count ),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<div class="comment-list">
			<?php
			//wp_list_comments('callback=better_comment&end-callback=better_comment_close');
			?>
		</div><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'mein-e-fahrzeug' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$fields =  array(
	    'author' => '<div class="form-group comment-form-author">' . '<label for="author">Ihr Name</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	        '<input id="author" name="author" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
	    'email'  => '<div class="form-group comment-form-email"><label for="email">Ihre Mail Adresse</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	        '<input id="email" name="email" type="text" class="form-control" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>'
	);

	global $wp_query;
	
	$notes = ($wp_query->query_vars['page_id'] > 0) ? 'Eigene Bewertung schreiben' : 'Einen Kommentar verfassen';
	$rating = ($wp_query->query_vars['page_id'] > 0) ? '<div class="row">
	    	<div class="bar-rating clearfix">
	    		<div class="row">
				    <div class="col-sm-6">
				    	<label for="rating">Bewertung hinzufügen</label>
				    </div>
				    <div class="col-sm-6 pull-right">
				    	<div class="text-right">
					    	<select class="" id="rating" name="rating">
					    		<option value="1">1</option>
					    		<option value="2">2</option>
					    		<option value="3">3</option>
					    		<option value="4">4</option>
					    		<option value="5">5</option>
					    	</select>
					    </div>
				    </div>
				</div>
			</div>
	    </div>' : '';
	 
	$comments_args = array(
	    'fields' =>  $fields,
	    'label_submit' => 'Bewertung absenden',
	    'comment_notes_before' => '<h3>' . $notes . '</h3>',
	    'comment_field' => '
	    <div class="form-group">
	    <input type="hidden" id="context" name="context" value="" />
	    <input type="hidden" id="context_id" name="context_id" value="" />
	    ' . $rating . '
	    </div>
	    <div class="form-group comment-form-comment"><label for="comment">Ihr Kommentar:</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" class="form-control"></textarea></div>'
	);

	comment_form($comments_args);
	?>

</div><!-- #comments -->

<?php

	function better_comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		$content = get_comment();
		$meta = get_comment_meta($content->comment_ID, 'rating');
		$rating = isset($meta[0]) ? $meta[0] : 0;
		extract($args, EXTR_SKIP);
		
		if ( 'article' == $args['style'] ) {
			$tag = 'article';
			$add_below = 'comment';
		} else {
			$tag = 'article';
			$add_below = 'comment';
		}

		$width = $rating * 20;

		?>
			<div class="comment-item">
				<div class="comment-meta post-meta" role="complementary">
					<div class="row">
						<div class="col-sm-4">
							<div class="ratings-wrapper">
								<div class="ratings">
			                        <div class="empty-stars"></div>
			                        <div class="full-stars" data-value="<?php echo $rating; ?>" style="width:<?php echo $width;?>%"></div>
			                    </div>
		                    </div>
						</div>
						<div class="col-sm-8">
							<h2 class="comment-author">
								<a class="comment-author-link" href="<?php comment_author_url(); ?>" itemprop="author"><?php comment_author(); ?></a> am 
								<time class="comment-meta-item" datetime="<?php comment_date('Y-m-d') ?>T<?php comment_time('H:iP') ?>" itemprop="datePublished"><?php comment_date('jS F Y') ?>, <a href="#comment-<?php comment_ID() ?>" itemprop="url"><?php comment_time() ?></a></time>
							</h2>
						</div>
					</div>
					
					<?php edit_comment_link('<p class="comment-meta-item">Edit this comment</p>','',''); ?>
					<?php if ($comment->comment_approved == '0') : ?>
					<p class="comment-meta-item">Your comment is awaiting moderation.</p>
					<?php endif; ?>
				</div>
				<div class="comment-content post-content" itemprop="text">
					<?php comment_text() ?>
				</div>
			</div>
		<?php }

	// end of awesome semantic comment

	function better_comment_close() {
		echo '';
}
?>