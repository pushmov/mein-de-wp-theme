<?php
	global $wp_query;
	define('MAX_ITEM', 100);
	$is_city = $is_country = false;
	$place = (isset($wp_query->query['place'])) ? $wp_query->query['place'] : '';
	$page_num = isset($wp_query->query['pagex']) ? $wp_query->query['pagex'] : 1;
	$cities = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_location WHERE city_permalink = '{$place}'" );
	if ( $cities[0]->total > 0 ) {
		$is_city = true;
	}

	$country_param = str_replace('-', ' ', $place);
	$countries = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_countries WHERE country_name = '{$country_param}'" );
	if ( $countries[0]->total > 0 ) {
		$is_country = true;
	}

	$param = (!isset($wp_query->query['place'])) ? '' : esc_html(urldecode($wp_query->query['place']));
	$paramurl = $param;
	if ($is_country) {
		$country = $wpdb->get_results( "SELECT * FROM parser_countries WHERE country_name = '{$country_param}'" );
		$headline = $country[0]->country_name;
		$param = "WHERE country_id = '{$country[0]->country_id}' AND status = 'ok'";
		$permalink = $country_param;
	} else {
		$param = "WHERE city_permalink = '{$place}' AND status = 'ok'";
		$city = $wpdb->get_results( "SELECT * FROM parser_location WHERE city_permalink = '{$place}' LIMIT 1" );
		$headline = (count($city) > 0) ? $city[0]->city : '';
		$permalink = $place;
	}

	$page_num = isset($wp_query->query['pagex']) ? $wp_query->query['pagex'] : 1;
	//country = 1, city = 2
	$target = '3';
	$locations = $wpdb->get_results( "SELECT parser_location.*,parser_station.status FROM parser_location INNER JOIN parser_station ON parser_location.place_id = parser_station.place_id " . $param . " LIMIT 0," . MAX_ITEM);
	$offset = ($page_num == 1 || $page_num === null) ? 0 : MAX_ITEM * ($page_num - 1);

	if ($permalink == '') {
		//list all
		$param = '';
	} else {
		if ($target == '1') {
			$country = $wpdb->get_results( "SELECT * FROM parser_countries WHERE country_name = '{$permalink}'" );
			$headline = $country[0]->country_name;
			$param = "WHERE country_id = '{$country[0]->country_id}'";
		} else if ($target == '2') {
			$param = "WHERE city_permalink = '{$permalink}'";
			$city = $wpdb->get_results( "SELECT * FROM parser_location WHERE city_permalink = '{$permalink}' LIMIT 1" );
			$headline = $city[0]->city;
		}
	}

	if ($target == '1' || $target == '2') {
		$total_list = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_location " . $param );
		$pages = ceil($total_list[0]->total / MAX_ITEM);
		$max = $pages;
		$locations = $wpdb->get_results( "SELECT parser_location.*,parser_station.status FROM parser_location INNER JOIN parser_station ON parser_location.place_id = parser_station.place_id " . $param . " LIMIT {$offset}," . MAX_ITEM);
		$link_base = 'stromtankstellen';
	} else {
		if ($permalink != '') {
			$param = urldecode($permalink);
			$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
			$spec_permalink = htmlspecialchars($param);
			$sub = '(SELECT *, (REPLACE( `network`, "\'", "" )) AS `custom` FROM parser_station ) AS tmp';
			$total_list = $wpdb->get_results( " SELECT COUNT(*) AS total FROM ".$sub." INNER JOIN parser_location ON parser_location.place_id = tmp.place_id WHERE (tmp.network = '{$param}' OR tmp.network = '{$spec_permalink}' OR tmp.custom = '{$param}' OR tmp.custom = '{$spec_permalink}') AND tmp.status = 'ok'" );
			$pages = ceil($total_list[0]->total / MAX_ITEM);
			$max = $pages;

			$locations = $wpdb->get_results( "SELECT * FROM ".$sub." INNER JOIN parser_location ON parser_location.place_id = tmp.place_id WHERE (tmp.network = '{$param}' OR tmp.network = '{$spec_permalink}' OR tmp.custom = '{$param}' OR tmp.custom = '{$spec_permalink}') AND tmp.status = 'ok' LIMIT {$offset}," . MAX_ITEM);

			$link_base = 'stromtankstellen';
		} else {
			$total_list = $wpdb->get_results( " SELECT COUNT(DISTINCT(network)) AS total FROM parser_station WHERE network <> '' AND status = 'ok'" );
			$pages = ceil($total_list[0]->total / MAX_ITEM);
			$max = $pages;
			$stations = $wpdb->get_results("SELECT DISTINCT(network) FROM parser_station WHERE network <> '' AND status = 'ok' ORDER BY network ASC LIMIT {$offset}," . MAX_ITEM);
			$locations = array();
			$delimiter = '-';
			foreach ($stations as $s) {
				$slug = str_replace('-', '--', $s->network);
				$slug = str_replace(' ', '-', $slug);
				$slug = str_replace('/', '-', $slug);
				$row = new StdClass();
				$row->name_permalink = $slug;
				$row->name = $s->network;
				$locations[] = $row;
			}
			$link_base = 'verbund';
		}

	}

	$pages_ellipsis = paginate_mein("#", "", $pages, $page_num, $radius = 2);
	$permalink_url = ($permalink == '') ? '' : $permalink.'/';
	get_header('verbund');
?>
<section class="ct-box">
</section>
<section class="ct-stromtankstellen e-station-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="box box-shadow station-list" id="station_list" data-target="3" data-permalink="<?php echo $paramurl; ?>">

					<h1>Alle Ladestationen <?php echo ($permalink == '' || $target < 3) ? '' : 'von '.urldecode($permalink);?></h1>
					<?php if (count($locations) > 0) :?>
						<ul class="list-unstyled e-station-list">
							<?php foreach ($locations as $loc) :?>
							<li><a href="<?php echo get_home_url(); ?>/<?php echo $link_base; ?>/<?php echo htmlspecialchars($loc->name_permalink);?>/"><?php echo $loc->name; ?></a></li>
							<?php endforeach; ?>
						</ul>

						<div class="text-center e-station-list-page">
							<ul class="list-unstyled pagination">
								<?php if ($page_num > 1) :?>
								<li class="first"><a href="<?php echo get_home_url(); ?>/verbund/<?php echo $permalink_url; ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
								<li class="prev"><a href="<?php echo get_home_url(); ?>/verbund/<?php echo $permalink_url; ?><?php echo ($page_num - 1) > 1 ? 'seite-'.($page_num - 1).'/' : '';?>" ><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
								<?php endif; ?>
								<?php

								// list display
								foreach ($pages_ellipsis as $page) {
								    // If page has a link
								    if (isset ($page['url'])) { ?>
								        <li><a href="<?php echo get_home_url(); ?>/verbund/<?php echo $permalink_url; ?><?php echo ($page['text'] > 1) ? 'seite-'.$page['text'].'/' : '';?>" >
										<?php echo $page['text'] ?>
									</a></li>
								<?php }
								    // no link - just display the text
								     else
								     	if ($page['text'] == $page_num) {
								     		echo '<li class="active"><a href="#">'.$page['text'].'</a></li>';
								     	} else {
								     		echo '<li><a href="#" class="ellipsis">'.$page['text'].'</a></li>';
								     	}

								} ?>

								<?php if ($page_num < $max) : ?>
									<li class="next"><a href="<?php echo get_home_url(); ?>/verbund/<?php echo $permalink_url; ?>seite-<?php echo ($page_num + 1); ?>/"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									<li class="last"><a href="<?php echo get_home_url(); ?>/verbund/<?php echo $permalink_url; ?>seite-<?php echo $max; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>

				</div>
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
