$(window).bind("pageshow", function() {
	$('#city_name').val('');
	$('#city_permalink').val('');
});
$(document).ready(function(){
	$('.selectpicker').selectpicker({
		iconBase: 'fontawesome',
		tickIcon: 'fa fa-check'
	});
});
$(function() {
	if ($('#rating').length > 0) {
		$('#rating').barrating({
			theme: 'css-stars'
		});
	}
});

if ($('.owl-carousel').length > 0) {
	$('.owl-carousel').owlCarousel({
	    items:1,
	    lazyLoad:true,
	    loop:true,
	    margin:10,
	    nav: true,
  		navText: ['<i class="fa fa-chevron-left fa-4x"></i>','<i class="fa fa-chevron-right fa-4x"></i>'],
  		dots: false
	});
}

if ($('#vehicle_result').length > 0) {
	function callAjax(param){
		$('#vehicle_result').html(loader);
		$.ajax({
			type: 'POST',
			url: my_ajax_object.ajax_url,
			data: param,
			success: function(data){
				$('#vehicle_result').html(data);
			}
		});
	}


	var loader = '<div class="box box-shadow box-overview" style="padding-top: 30px;padding-bottom:30px"><div class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div></div>';
	var form = $('#vehicle_filter').serializeArray();
	form.push({name: 'action', value: 'get_vehicle_overview'});
	form.push({name: 'pagenum', value: 1});
	form.push({name: 'price', value: ''});
	form.push({name: 'range', value: ''});
	callAjax(form);

	$('#vehicle_result').on('click', '.pagination a.page', function(e){
		e.preventDefault();
		var form = $('#vehicle_filter').serializeArray();
		form.push({name: 'action', value: 'get_vehicle_overview'});
		form.push({name: 'pagenum', value: $(this).data('page')});
		callAjax(form);
	});

	$('.vehicle-overview .selectpicker').on('change', function (e) {
		var form = $('#vehicle_filter').serializeArray();
		form.push({name: 'action', value: 'get_vehicle_overview'});
		form.push({name: 'pagenum', value: 1});
		callAjax(form);
	});

	$('#vehicle_filter #car_maker').on('change', function(){
		var str = $(this).val();
		if (str.indexOf('alle') >= 0) {
			$('#car_maker').selectpicker('deselectAll');
		}
	});
}

if ($('#location_search').length > 0) {
	$('#location_search').easyAutocomplete({
		url: my_ajax_object.ajax_url+'?action=get_location',
		getValue: "name",
		list: {
			match: {
				enabled: true
			},
			onClickEvent: function(){
				var value = $('#location_search').getSelectedItemData();
				$('#city_name').val(value.name);
				$('#city_permalink').val(value.code);
			},
			onKeyEnterEvent: function(){
				var value = $('#location_search').getSelectedItemData();
				$('#city_name').val(value.name);
				$('#city_permalink').val(value.code);
			}
		},
		
	});
}

if ( $('.location-search').length > 0 ) {
	$('.location-search #search').on('click', function(e){
		e.preventDefault();
		if ($('#city_permalink').val() == '') {
			return false;
		}
		window.location.href = '/stromtankstellen-list/' + $('#city_permalink').val() + '/';
	});

	$('form.location-search input').keypress(function(e){
		if(e.which == 13) {
			if ($('#city_permalink').val() === '') {
				return false;
			}
		}
	});

	$(document).on('submit', 'form.location-search', function(e){
		e.preventDefault();
		window.location.href = '/stromtankstellen-list/' + $('#city_permalink').val() + '/';
	});
}


if ( $('#station_list').length > 0 ) {

	$('#station_list').on('click', '.pagination a.page', function(e){
		e.preventDefault();
		var param = [];
		param.push({name: 'action', value: 'get_location_list'});
		param.push({name: 'target', value: $('#station_list').attr('data-target')});
		param.push({name: 'permalink', value: $('#station_list').attr('data-permalink')});
		param.push({name: 'pagenum', value: $(this).data('page')});
		$.ajax({
			type: 'POST',
			url: my_ajax_object.ajax_url,
			data: param,
			success: function(data){
				$('#station_list').html(data);
			}
		});
	});
	

	$('#station_list').on('click', '.pagination a.ellipsis', function(e){
		e.preventDefault();
		return false;
	});
}

$('#eauto_submit').on('click', function(e){
	e.preventDefault();
	var form = $(this).closest('form');
	var v = form.find('[name="v"]').val();
	var t = form.find('[name="t"]').val();
	if ( v == '' &&  t == '') {
		window.location.href = '/elektroauto/';
	} else {
		window.location.href = '/elektroauto/?v=' + v + '&t=' + t;
	}
});

if ($('#map_reveal').length > 0) {
	$('#map_reveal').on('click', function(e){
		var btn = $(this);
		e.preventDefault();
		$('.ct-map-wrapper').animate({
			'min-height': "+=550"
		}, 500, 'swing', function(){
			$('.ct-map-wrapper img').addClass('hide');
			$('#map_layer').show();
			$('#map_reveal').hide();
			initMap(btn.attr('data-lat'), btn.attr('data-lng'));
		});
	});
}

function initMap(lat, lng) {
	var lat = parseFloat(lat);
	var lng = parseFloat(lng);
	var myLatLng = {lat: lat, lng: lng};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		scale: 2,
		center: myLatLng,
		mapTypeControl: false,
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.LEFT_CENTER
		}
	});

	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title: 'Title'
	});

	var directionsDisplay = new google.maps.DirectionsRenderer;
	var directionsService = new google.maps.DirectionsService;
	
	$('.transportation .btn').on('click', function(e) {
		directionsDisplay.setMap(map);
		e.preventDefault();
        calculateAndDisplayRoute(directionsService, directionsDisplay, lat, lng, $(this).attr('id'));
    });

    $('#show_route').on('click', function(e){
    	directionsDisplay.setMap(map);
		e.preventDefault();
        calculateAndDisplayRoute(directionsService, directionsDisplay, lat, lng, 'DRIVING');
    });
	
}


function calculateAndDisplayRoute(directionsService, directionsDisplay, dest_lat, dest_lng, selectedMode) {

	var start_address = $('#start_address').val();
	if (start_address == '') {
		alert('Start Address Required!');
		$('#start_address').focus();
		return false;
	}
	var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': start_address}, function(results, status) {
      if (status == 'OK') {
        //map.setCenter(results[0].geometry.location);
        directionsService.route({
			origin: results[0].geometry.location,  // Haight.
			destination: {lat: dest_lat, lng: dest_lng},  // Ocean Beach.
	        // Note that Javascript allows us to access the constant
	        // using square brackets and a string value as its
	        // "property."
	        travelMode: google.maps.TravelMode[selectedMode]
	    }, function(response, status) {
	        if (status == 'OK') {
	         	directionsDisplay.setDirections(response);
	        } else {
	          	window.alert('Directions request failed due to ' + status);
	        }
	    });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
	
}

$('#close_map').on('click', function(e){
	e.preventDefault();
	$('.ct-map-wrapper').animate({
		'min-height': "-=550"
	}, 500, 'swing', function(){
		$('.ct-map-wrapper img').removeClass('hide');
		$('#map_layer').hide();
		$('#map_reveal').show();
	});
});

function fbShare(url, title, descr, image, winWidth, winHeight) {
	var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width='+winWidth+',height='+winHeight);
}

$('.fb').on('click', function(e){
	e.preventDefault();
	fbShare($(this).data('url'), $(this).data('title'), $(this).data('description'), $(this).data('img'), 520, 350);
});

$('.tw').on('click', function(e){
	e.preventDefault();
	window.open('http://twitter.com/intent/tweet?text='+$(this).data('text')+'&url='+$(this).data('url'),'tweet','height=300,width=550,resizable=1');return false;
});