<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mein-e-fahrzeug
 */

get_header();
?>
	<section class="ct-search">
		<div class="container">
			<div class="box box-shadow">
				<div class="row">
					<div class="col-md-6">
						<h2>25.000 Ladesäulen online</h2>
						<form>
							<h3>E-Tankstellen finden</h3>
							<input type="" name="">
							<button type="button" class="">SUCHEN</button>
							<h5>» Zum E-Tankstellen Verzeichnis</h5>
						</form>
						<ul>
							<li><a href="#">Welche Ladesäulen Typen gibt es ?</a></li>
							<li><a href="#">Wie bezahlen?</a></li>
							<li><a href="#">Welche Ladesäulen Typen gibt es ?</a></li>
							<li><a href="#">Wie bezahlen?</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<img src="<?php echo get_template_directory_uri(); ?>/img/banner.jpg" class="img-responsive">
					</div>
				</div>
			</div>
			<div class="box box-shadow">
				<div class="row">
					<div class="col-md-4">
						<h2>Kaufberatung E-Auto</h2>
						<h5>Wieviel ist Dein E-Auto wert?</h5>
						<select>
							<option>1</option>
							<option>2</option>
							<option>3</option>
						</select>
						<select>
							<option>1</option>
							<option>2</option>
							<option>3</option>
						</select>
						<button type="button" class="">E-Autos anzeigen</button>
					</div>
					<div class="col-md-8">
						<img src="<?php echo get_template_directory_uri(); ?>/img/car.png" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ct-news">
		<div class="container">
			<div class="col-md-9">

				<?php
				if ( have_posts() ) :

					if ( is_home() && ! is_front_page() ) :
						?>
						<header>
							<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
						</header>
						<?php
					endif;

					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content-news-custom', get_post_type() );

					endwhile;

					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

				
			</div>
			<div class="col-md-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/sidebar.jpg" class="img-responsive">
			</div>
		</div>
	</section>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
