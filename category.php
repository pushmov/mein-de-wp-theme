<?php

get_header();

?>
<section class="ct-box">
</section>
<section class="ct-news">
		<div class="container">
			<div class="col-md-9">
				<?php 	
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		            $args_latest = array(           
			            'post_type' => 'post',
			            'ignore_sticky_posts' => 1,
			            'posts_per_page' => 5,
			            'paged' => $paged
		        	);
            		$the_query = new WP_Query($args_latest); ?>
		            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		            <?php get_template_part( 'template-parts/content-news-custom', $the_query -> get_post_type() );?>
		            <?php endwhile;?>
					<?php wp_reset_query();?>

					<div class="pull-left">
						<?php next_posts_link( '&larr; Older posts', $wp_query ->max_num_pages); ?>
					</div>

					<div class="pull-right">
						<?php previous_posts_link( 'Newer posts &rarr;' ); ?>
					</div>
					
					
			</div>
			<div class="col-md-3">
				<?php get_sidebar('news'); ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>