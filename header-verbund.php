<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mein-e-fahrzeug
 */
global $post;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php 
		global $wp_query;
		global $wp;
		if ( !isset($wp_query->query['pagex']) || $wp_query->query['pagex'] == 1 ) : ?>
	<meta name="robots" content="index,follow" />
	<link rel="canonical" href="<?php echo home_url( $wp->request ); ?>/" />
	<?php else : ?>
	<meta name="robots" content="noindex,follow" />
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<nav class="navbar navbar-default">
        <div class="container column">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              		<span class="sr-only">Toggle navigation</span>
              		<span class="icon-bar"></span>
              		<span class="icon-bar"></span>
              		<span class="icon-bar"></span>
            	</button>
            	<a class="navbar-brand logo" href="<?php echo get_home_url(); ?>/"><img alt="mein-e-fahrzeuge.de" src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>
          	</div>
          	<div id="navbar" class="navbar-collapse collapse">
              <?php
              $menu_locations = wp_get_nav_menu_items('main_menu');
              ?>
              <?php if (!empty($menu_locations)) :?>
                <ul class="nav navbar-nav navbar-right">
                  <?php foreach ($menu_locations as $menu) :?>
                    <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                  <?php endforeach;?>
                </ul>
              <?php endif; ?>
          	</div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
	</nav>