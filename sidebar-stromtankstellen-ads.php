<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mein-e-fahrzeug
 */

if ( ! is_active_sidebar( 'sidebar-7' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area widget-stromtankstellen-box">
	<?php dynamic_sidebar( 'sidebar-7' ); ?>
</aside><!-- #secondary -->
