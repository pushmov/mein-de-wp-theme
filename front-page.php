<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mein-e-fahrzeug
 */

get_header();
?>
	<section class="ct-box"></section>
	<section class="ct-search">
		<div class="container">
			<div class="box box-shadow e-auto-search">
				<div class="row">
					<div class="col-md-6">
						<?php $custom = get_post_custom();?>
						<h2><?php echo isset($custom['headline']) ? $custom['headline'][0] : ''; ?></h2>
						<form class="form-inline location-search">
							<h3><?php echo isset($custom['sub_headline']) ? $custom['sub_headline'][0] : ''?></h3>
							<div class="input-group input-group-txt">
								<input type="text" name="location_search" id="location_search" placeholder="Ort bzw. Adresse" class="form-control">
								<div class="input-group-addon"><button type="button" id="search" class="btn btn-primary"><i class="fa fa-search"></i>SUCHEN</button></div>
								<input type="hidden" name="city_permalink" value="" id="city_permalink">
								<input type="hidden" name="city_name" value="" id="city_name">
							</div>
							<a href="<?php echo get_home_url(); ?>/stromtankstellen-list/">&raquo; Zum E-Tankstellen Verzeichnis</a>
						</form>
						<ul>
							<?php if (isset($custom['link_1_target'][0]) && isset($custom['link_1_label'][0])) : ?>
							<li><a href="<?php echo $custom['link_1_target'][0]; ?>"><?php echo $custom['link_1_label'][0]; ?></a></li>
							<?php endif; ?>

							<?php if (isset($custom['link_2_target'][0]) && isset($custom['link_2_label'][0])) : ?>
							<li><a href="<?php echo $custom['link_2_target'][0]; ?>"><?php echo $custom['link_2_label'][0]; ?></a></li>
							<?php endif; ?>

							<?php if (isset($custom['link_3_target'][0]) && isset($custom['link_3_label'][0])) : ?>
							<li><a href="<?php echo $custom['link_3_target'][0]; ?>"><?php echo $custom['link_3_label'][0]; ?></a></li>
							<?php endif; ?>

							<?php if (isset($custom['link_4_target'][0]) && isset($custom['link_4_label'][0])) : ?>
							<li><a href="<?php echo $custom['link_4_target'][0]; ?>"><?php echo $custom['link_4_label'][0]; ?></a></li>
							<?php endif; ?>
						</ul>
					</div>
					<div class="col-md-6">
						<?php if (isset($custom['e_station_image'])) :?>
							<?php $img = wp_get_attachment_image_src($custom['e_station_image'][0], 'full');?>
							<img src="<?php echo $img[0] ?>" class="img-responsive">
						<?php else : ?>
							<img src="<?php echo get_template_directory_uri(); ?>/img/banner.jpg" class="img-responsive">
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="box box-shadow e-auto-dropdown">
				<div class="row">
					<div class="col-md-4">
						<form method="get" action="elektroauto">
							<h2><?php echo isset($custom['headline_2']) ? $custom['headline_2'][0] : ''; ?></h2>
							<h5><?php echo isset($custom['sub_headline_2']) ? $custom['sub_headline_2'][0] : ''; ?></h5>
							<?php $car_makers = $wpdb->get_results("SELECT DISTINCT(car_maker) AS vendor FROM parser_evdatabase_vehicles WHERE car_maker IS NOT NULL ORDER BY car_maker ASC"); ?>
							<?php if (count($car_makers) > 0) :?>
							<select class="selectpicker" title="Marke auswählen" name="v">
								<?php foreach ($car_makers as $car) :?>
								<option value="<?php echo $car->vendor; ?>"><?php echo $car->vendor; ?></option>
								<?php endforeach; ?>
							</select>
							<?php endif; ?>
							<select class="selectpicker" title="Antrieb" name="t">
								<option value="electric">E-Fahrzeug</option>
								<option value="hybrid">Plug-in-Hybrid</option>
							</select>
							<button type="submit" id="eauto_submit" class="btn btn-primary btn-e-auto-dropdown">E-Autos anzeigen<i class="fa fa-caret-right"></i></button>
						</form>
					</div>
					<div class="col-md-8">
						<?php if (isset($custom['car_image'])) :?>
							<?php $img = wp_get_attachment_image_src($custom['car_image'][0], 'full');?>
							<img src="<?php echo $img[0] ?>" class="img-responsive">
						<?php else : ?>
						<img src="<?php echo get_template_directory_uri(); ?>/img/car.png" class="img-responsive">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ct-news">
		<div class="container">
			<div class="col-md-9">
				<?php 	
		            $args_latest = array(           
			            'post_type' => 'post',
			            'ignore_sticky_posts' => 1,
			            'posts_per_page' => 3       
		        	);
            		$the_query = new WP_Query($args_latest); ?>
		            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
		            <?php get_template_part( 'template-parts/content-news-custom', $the_query -> get_post_type() );?>
		            <?php endwhile;?>
					<?php wp_reset_query();?>
			</div>
			<div class="col-md-3 text-center">
				<?php get_sidebar('home');?>
			</div>
		</div>
	</section>

<?php
//get_sidebar();
get_footer();
