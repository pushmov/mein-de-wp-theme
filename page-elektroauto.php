<?php
	global $wp_query;
	get_header();
	define('MAX_ITEM', 10);

	$page_id = $wp_query->query_vars['page_id'];
	$page_num = isset($wp_query->query_vars['pagenum']) ? $wp_query->query_vars['pagenum'] : 1;
	$offset = ($page_num == 1) ? 0 : MAX_ITEM * ($page_num - 1);

	// ... more ...
	$total_vehicles = $wpdb->get_results("SELECT COUNT(*) AS total FROM parser_evdatabase_vehicles");
	$pages = ceil($total_vehicles[0]->total / MAX_ITEM);
	$myrows = $wpdb->get_results( "SELECT * FROM parser_evdatabase_vehicles LIMIT {$offset}," . MAX_ITEM );
	$custom = get_post_custom();

	if ($page_num > $pages) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}

	$car_makers = $wpdb->get_results("SELECT DISTINCT(car_maker) AS vendor FROM parser_evdatabase_vehicles WHERE car_maker IS NOT NULL ORDER BY car_maker ASC");

	$v = isset($wp_query->query_vars['v']) ? $wp_query->query_vars['v'] : '';
	$t = isset($wp_query->query_vars['t']) ? $wp_query->query_vars['t'] : '';

	$myrows = $wpdb->get_results( "SELECT * FROM parser_evdatabase_vehicles" );
?>
<section class="ct-box"></section>
<section class="ct-vehicle">
		<div class="container">
			
			<div class="row vehicle-overview">
				<div class="col-md-9">
					<div class="filter">
						<form id="vehicle_filter" class="form-inline">
							<div class="row">
								<div class="col-md-5ths">
									<label>Filtern nach: </label>
								</div>
								<div class="col-md-5ths">
									<?php if (count($car_makers) > 0) :?>
									<select name="car_maker[]" class="form-control selectpicker" title="Hersteller" multiple="multiple" id="car_maker">
										<option value="alle">Alle</option>
										<?php foreach($car_makers as $maker) :?>
										<option <?php echo ($maker->vendor == $v) ? 'selected="selected"' : ''; ?>value="<?php echo $maker->vendor; ?>"><?php echo $maker->vendor; ?></option>
										<?php endforeach; ?>
									</select>
									<?php endif;?>
								</div>
								<div class="col-md-5ths">
									<select name="type" class="selectpicker" title="Antrieb">
										<option value="" selected="selected">Alle</option>
										<option <?php echo ($t == 'electric') ? 'selected="selected"' : ''; ?> value="electric">Voll-Elektrisch</option>
										<option <?php echo ($t == 'hybrid') ? 'selected="selected"' : ''; ?> value="hybrid">Plugin-Hybrid</option>
									</select>
								</div>
								<div class="col-md-5ths">
									<select name="price" title="Preis" class="selectpicker">
										<option value="descending">Absteigend</option>
										<option value="ascending">Aufsteigend</option>
									</select>
								</div>
								<div class="col-md-5ths">
									<select name="range" class="selectpicker" title="Reichweite">
										<option value="descending">Absteigend</option>
										<option value="ascending">Aufsteigend</option>
									</select>
								</div>
							</div>
						</form>
						<div class="text-center">
							
						</div>
					</div>
				</div>
			</div>
			<?php get_sidebar('top');?>
			<div class="row vehicle-overview">
				<div class="col-md-9">

					<?php if (isset($custom['headline_1'])):?>
						<h1><?php echo $custom['headline_1'][0];?></h1>
					<?php endif; ?>

					<div id="vehicle_result"></div>

					<?php if (isset($custom['headline_2'])) :?>
					<h2><?php echo $custom['headline_2'][0];?></h2>
					<?php endif; ?>
					<div class="box box-shadow beispieltext">
						<?php echo $custom['vehicle_overview_text'][0];?>
					</div>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
			<div class="hide">
				<?php if (!empty($myrows)) {
					foreach ($myrows as $row) : 
						$delimiter = '-';
						$slug = str_replace('-', '--', strtolower($row->car_name));
						$slug = str_replace(' ', '-', $slug);
						?>
						<a href="<?php echo get_home_url(); ?>/elektroauto/<?php echo $slug;?>/"><?php echo $row->car_name; ?></a>
					<?php endforeach;
				} ?>
			</div>
		</div>
	</section>

<?php get_footer();?>