<?php
	global $wp_query;
	$is_city = $is_country = false;
	$place = urldecode($wp_query->query['place']);
	$page_num = isset($wp_query->query['pagex']) ? $wp_query->query['pagex'] : 1;

	$cities = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_location WHERE city_permalink = '{$place}'" );
	if ( $cities[0]->total > 0 ) {
		$is_city = true;
	}
	
	$country_param = str_replace('-', ' ', $place);
	$countries = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_countries WHERE country_name = '{$country_param}'" );
	if ( $countries[0]->total > 0 ) {
		$is_country = true;
	}

	if ( $is_city || $is_country ) {
		/** use page-stromtankstellen-list instead */
		$trail = ($place == '') ? '' : $place.'/';
		header("Location:/stromtankstellen-list/".$trail);
		exit();
		require_once( get_template_directory() . '/template-parts/stromtankstellen-list.php' );
	} else {
		require_once( get_template_directory() . '/template-parts/stromtankstellen-place.php' );
	}
?>
