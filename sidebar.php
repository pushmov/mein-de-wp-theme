<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mein-e-fahrzeug
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}


?>

<aside id="secondary" class="widget-area widget-elektroauto">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->