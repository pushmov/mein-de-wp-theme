<?php
	global $wp_query;
	get_header();


	$car = str_replace('--', '%', $wp_query->query['pagenum']);
	$car = str_replace('-', ' ', $car);

	if (substr_count($car, '%') > 1) {
		$car = str_replace('%%', ' - ', $car);
	}
	$car = str_replace('%', '-', $car);
	$car = ucwords($car);

	$myrows = $wpdb->get_results( "SELECT * FROM parser_evdatabase_vehicles WHERE car_name = '{$car}' LIMIT 0,1" );
	
	if (count($myrows) <= 0) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}

	$comments = get_comments();

	$data = current($myrows);
	$imgs = unserialize($data->image_url);
?>

<section class="ct-box">
	<div class="container">
		
	</div>
</section>
<section class="ct-detail vehicle-detail">
	<div class="container">
		<div class="breadcrumb-nav">
			<div class="clearfix">
				<div class="col-md-12">
					<div class="nav nav-vehicle">
						<ol class="list-unstyled" itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope 
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing" 
									itemprop="item" href="<?php echo get_home_url(); ?>/">
									<i class="fa fa-home fa-2x"></i>
									<span class="hide" itemprop="name">Home</span>
								</a>
								<meta itemprop="position" content="1" />
							</li>
							<li itemprop="itemListElement" itemscope 
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing" 
									itemprop="item" href="<?php echo get_home_url(); ?>/elektroauto/">
									<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name">Elektroautos</span></strong>
								</a>
								<meta itemprop="position" content="2" />
							</li>
							<li itemprop="itemListElement" itemscope
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing" 
									itemprop="item" href="#">
									<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $data->car_name; ?></span></strong>
								</a>
								<meta itemprop="position" content="3" />
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
			<div class="col-sm-9">
				<h1><?php echo $data->car_name; ?></h1>
				<div class="box box-shadow">
					<div class="row">
						<div class="col-sm-7">
							<?php if (!empty($imgs)) : ?>
							<div class="owl-carousel owl-theme">
								<?php foreach ($imgs as $img) : ?>
								<?php 
									if ($img == '') continue;
									$cdn_url = 'https://cdn.mein-e-fahrzeug.de/';
									$cdn_name = $cdn_url . basename($img);
								?>
								<img data-src-retina="<?php echo $cdn_name; ?>" alt="" data-src="<?php echo $cdn_name; ?>" class="box-shadow owl-lazy">
								<?php endforeach; ?>
							</div>
							<?php endif; ?>
						</div>
						<div class="col-sm-5">
							<div class="vehicle-detail-box box-shadow">
								<h3 class="text-center">Daten</h3>
								<ul class="list-unstyled">
									<li><span>Preis:</span>ab <?php echo number_format($data->car_price,0,",","."); ?> &euro;</li>
									<?php $kind = strstr($data->car_kind, 'Hybride') ? 'Plugin-Hybride' : 'Elektrisch'; ?>
									<li><span>Antriebsart:</span><?php echo $kind; ?></li>
									<?php
										$month = array(
											'Januari' => 'Januar',
											'Februari' => 'Februar',
											'Maart' => 'März',
											'April' => 'April',
											'Mei' => 'Mai',
											'Juni' => 'Juni',
											'Juli' => 'Juli',
											'Augustus' => 'August',
											'September' => 'September',
											'Oktober' => 'Oktober',
											'November' => 'November',
											'December' => 'Dezember'
										);
									?>
									<?php foreach ($month as $key => $val) :?>
										<?php if (strstr($data->available_from, $key)):?>
										<li><span>Lieferbar:</span>ab <?php echo str_replace($key, $val,$data->available_from); ?></li>
										<?php endif; ?>
									<?php endforeach; ?>
									<li><span>Reichweite:</span><?php echo ($data->electric_range == '') ? 'NA' : $data->electric_range.' km'; ?></li>
								</ul>
							</div>
								
						</div>
					</div>
				</div>

				<h3><i class="fa fa-caret-right"></i>Leistung</h3>
				<div class="box box-shadow">
					<div class="clearfix table">
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">
									<span class="e-auto-item full">Höchstgeschwindigkeit</span>
									<span class="e-auto-item break">Höchst-<br>geschwindigkeit</span>
								</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<span class="e-auto-item full"><?php echo ($data->top_speed != '') ? $data->top_speed.' km/h' : 'NA'; ?></span>
									<span class="e-auto-item break"><br><?php echo ($data->top_speed != '') ? $data->top_speed. ' km/h' : 'NA'; ?></span>
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">
									<span class="e-auto-item full">Beschleunigung 0-100 km/h</span>
									<span class="e-auto-item break">Beschleunigung<br>0-100 km/h</span>
								</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<span class="e-auto-item full"><?php echo ($data->acceleration != '') ? number_format($data->acceleration, 1,',','.').' Sek.' : 'NA';?></span>
									<span class="e-auto-item break"><br><?php echo ($data->acceleration != '') ? number_format($data->acceleration, 1,',','.').' Sek.' : 'NA';?></span>
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Max. Leistung</div>
								<div class="col-xs-6 col-sm-12 cell-item"><?php echo $data->power_hp; ?> PS (<?php echo $data->power_kw; ?> kW)</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Drehmoment</div>
								<div class="col-xs-6 col-sm-12 cell-item"><?php echo $data->torque; ?> Nm</div>
							</div>
						</div>
					</div>
				</div>

				<h3><i class="fa fa-caret-right"></i>Batterie und Laden</h3>
				<div class="box box-shadow">
					<div class="row">
						<div class="col-sm-6">
							<h3>Standard-Plug</h3>
							<ul class="list-unstyled vehicle-list">
								<li><span>Batteriekapazität:</span><?php echo number_format($data->battery_capacity, 1, ',', '.'); ?> kWh</li>
								<li><span>Ladetechnik:</span><?php echo str_replace('Type', 'Typ', $data->plug_normal); ?></li>
								<li><span>Ladekapazität:</span><?php echo number_format($data->load_capacity_normal, 1, ',','.');?> kW Wechselstrom</li>
								<li><span>Max. Ladezeit:</span><?php echo str_replace('m', 'min', str_replace('u', 'h', str_replace('uur', 'h', $data->charing_time_normal))); ?></li>
							</ul>
						</div>
						<div class="col-sm-6">
							<h3>Schnelllade-Plug</h3>
							<ul class="list-unstyled vehicle-list">
								<li><span>Ladetechnik:</span><?php echo str_replace('Type', 'Typ', str_replace('(Optie)', '', $data->plug_fast));?></li>
								<li><span>Ladekapazität:</span><?php echo $data->load_capacity_fast; ?> kW Gleichstrom</li>
								<li><span>Max. Ladezeit:</span><?php echo $data->charing_time_fast; ?></li>
							</ul>
						</div>
					</div>
				</div>

				<h3><i class="fa fa-caret-right"></i>Energieverbrauch</h3>
				<div class="box box-shadow">
					<div class="row">
						<div class="col-sm-6">
							<h3>NEFZ-Norm</h3>
							<ul class="list-unstyled vehicle-list">
								<li><span>Elektrische Reichweite:</span><?php echo ($data->action_radius_nedc == '') ? 'NA' : $data->action_radius_nedc.' km'; ?></li>
								<li><span>Energieverbrauch:</span><?php echo $data->consumption_vehicle_nedc == '' ? 'NA' : number_format($data->consumption_vehicle_nedc, 1, ',','.').' kWh/100 k';?></li>
							</ul>
						</div>
						<div class="col-sm-6">
							<h3>WLTP-Norm</h3>
							<ul class="list-unstyled vehicle-list">
								<li><span>Elektrische Reichweite:</span><?php echo ($data->action_radius_wltp == '') ? 'NA' : $data->action_radius_wltp.' km'; ?></li>
								<?php if ($data->consumption_vehicle_wltp != '') :?>
								<li><span>Energieverbrauch:</span><?php echo number_format((float)$data->consumption_vehicle_wltp, 1,',','.');?> kWh / 100 km</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>

				<?php if ($data->fuel != '') :?>
				<h3><i class="fa fa-caret-right"></i>Verbrennungsmotor</h3>
				<div class="box box-shadow">
					<div class="clearfix table">
						<div class="col-sm-6 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Verbrennungsmotor-Reichweite</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo ($data->action_radius_fuel == '') ? 'NA' : $data->action_radius_fuel.' km'; ?>
								</div>
							</div>
						</div>
						<div class="col-sm-6 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Kraftstoff</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo str_replace('Benzine', 'Benzin', $data->fuel);?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<h3><i class="fa fa-caret-right"></i>Maße und Gewicht</h3>
				<div class="box box-shadow">
					<div class="clearfix table">
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Antrieb</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo str_replace('AWD', 'Allrad', str_replace('Voor', 'Vorderachse', str_replace('Achter', 'Hinterachse', $data->impuls))); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Leergewicht</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo number_format($data->vehicle_mass, 0, ',', '.'); ?> kg
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Kofferraum Volumen</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo ($data->vehicle_baggage == 'Geen Data') ? '' : $data->vehicle_baggage.' Liter';?>
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Sitzplätze</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo $data->vehicle_seats; ?> Personen
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix table">
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Länge</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo number_format(($data->vehicle_lenght / 1000), 2, ',', '.');?> m
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Breite</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo number_format(($data->vehicle_width / 1000), 2, ',', '.');?> m
								</div>
							</div>
						</div>
						<div class="col-sm-3 border-bottom">
							<div class="row">
								<div class="col-xs-6 col-sm-12 cell">Höhe</div>
								<div class="col-xs-6 col-sm-12 cell-item">
									<?php echo number_format(($data->vehicle_height / 1000), 2, ',', '.');?> m
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php
				$widget_content = '';
				$sidebar_id = 'sidebar-2';
				$sidebars_widgets = wp_get_sidebars_widgets();
				$widget_ids = $sidebars_widgets[$sidebar_id]; 
				    foreach( $widget_ids as $id ) {
				        $wdgtvar = 'widget_'._get_widget_id_base( $id );
				        $idvar = _get_widget_id_base( $id );
				        $instance = get_option( $wdgtvar );
				        $idbs = str_replace( $idvar.'-', '', $id );
				        if ($instance[$idbs]['title'] == $myrows[0]->car_name) {
				        	$widget_content = $instance[$idbs]['content'];
				        }
				    }
				?>

				<?php if ($widget_content != '' ) : ?>
				<h3><i class="fa fa-caret-right"></i>Weitere Infos über den <?php echo $data->car_name; ?></h3>
				<div class="box box-shadow">
					<?php echo $widget_content; ?>
				</div>
				<?php endif; ?>
				
				<h3><i class="fa fa-caret-right"></i>User über <?php echo $data->car_name; ?></h3>
				<div class="box box-shadow">
					<div class="comment-list">
					<?php
					$elektroauto_single = get_page_by_path('elektroauto-single');
					$comment_args = apply_filters( 'comments_template_query_args', array(
						'post_id' => $elektroauto_single->ID,
						'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'context',
								'value' => 'vehicle',
								'compare' => '='
							),
							array(
								'key' => 'context_id',
								'value' => $data->vehicle_id,
								'compare' => '='
							)
						)
					) );
					$comment_query = new WP_Comment_Query;
					$comments = $comment_query->query($comment_args);

					if ($comments) {
						foreach ($comments as $comment) :
							$rating_meta = get_comment_meta($comment->comment_ID, 'rating');
							$rating = isset($rating_meta[0]) ? $rating_meta[0] : 0;

							$context_meta = get_comment_meta($comment->comment_ID, 'context');
							$context_id = get_comment_meta($comment->comment_ID, 'context_id');

							$width = $rating * 20;
							
							?>
							<div class="comment-item" id="comment-<?php echo $comment->comment_ID; ?>">
								<div class="comment-meta post-meta" role="complementary">
									<?php if ($comment->comment_approved != '0') : ?>
									<div class="row">
										<div class="col-sm-3">
											<div class="rating-wrapper">
												<div class="ratings">
							                        <div class="empty-stars"></div>
							                        <div class="full-stars" data-value="<?php echo $rating; ?>" style="width:<?php echo $width;?>%"></div>
							                    </div>
							                </div>
										</div>
										<div class="col-sm-9">
											<?php
												$month = array(
													'01' => 'Januar',
													'02' => 'Februar',
													'03' => 'März',
													'04' => 'April',
													'05' => 'Mai',
													'06' => 'Juni',
													'07' => 'Juli',
													'08' => 'August',
													'09' => 'September',
													'10' => 'Oktober',
													'11' => 'November',
													'12' => 'Dezember'
												);
												$gdate[] = date('d.', strtotime($comment->comment_date));
												$gdate[] = $month[date('m', strtotime($comment->comment_date))];
												$gdate[] = date('Y', strtotime($comment->comment_date));

												$gtime[] = date('H:i', strtotime($comment->comment_date));
												$gtime[] = 'Uhr';
											?>
											<h2 class="comment-author"><em><?php comment_author(); ?> am 
												<time class="comment-meta-item" datetime="<?php comment_date('Y-m-d') ?>T<?php comment_time('H:iP') ?>" itemprop="datePublished"><?php echo join(' ', $gdate); ?>, <?php echo join(' ', $gtime); ?></time>
											</em></h2>
										</div>
									</div>
									<?php endif; ?>
									
									<?php //edit_comment_link('<p class="comment-meta-item">Edit this comment</p>','',''); ?>
									<?php if ($comment->comment_approved == '0') : ?>
									<p class="comment-meta-item">Your comment is awaiting moderation.</p>
									<?php endif; ?>
								</div>
								<div class="comment-content post-content" itemprop="text">
									<?php if ($comment->comment_approved != '0') : ?>
									<?php comment_text() ?>
									<?php endif; ?>
								</div>
							</div>
							<?php
						endforeach;
					}

					?>
					</div>
					<?php comments_template(); ?>
				</div>
				
			</div>
			<div class="col-sm-3">
				<?php get_sidebar(); ?>
			</div>
	</div>

</section>

<?php get_footer(); ?>
<script type="text/javascript">
	jQuery('#context').val('vehicle');
	jQuery('#context_id').val('<?php echo $data->vehicle_id; ?>');
</script>