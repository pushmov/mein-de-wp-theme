<?php
/**
 * mein-e-fahrzeug functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mein-e-fahrzeug
 */

function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
}
//add_action( 'after_setup_theme', 'limit_text' );

if ( ! function_exists( 'mein_e_fahrzeug_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mein_e_fahrzeug_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on mein-e-fahrzeug, use a find and replace
		 * to change 'mein-e-fahrzeug' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mein-e-fahrzeug', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'mein-e-fahrzeug' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mein_e_fahrzeug_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'mein_e_fahrzeug_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mein_e_fahrzeug_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mein_e_fahrzeug_content_width', 640 );
}
add_action( 'after_setup_theme', 'mein_e_fahrzeug_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mein_e_fahrzeug_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Vehicle Overview Top Bar', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-6',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Vehicle Detail Sidebar', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Vehicle Details - Free Text Box', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget box box-shadow %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Homepage Sidebar', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget box box-shadow %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Stromtankstellen Page Adsense', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-7',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Stromtankstellen Page Sidebar', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-4',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'News Page Sidebar', 'mein-e-fahrzeug' ),
		'id'            => 'sidebar-5',
		'description'   => esc_html__( 'Add widgets here.', 'mein-e-fahrzeug' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'mein_e_fahrzeug_widgets_init' );

require_once( get_template_directory() . '/classes/class-wp-recent-comments-custom.php' );
require_once( get_template_directory() . '/classes/class-wp-recent-news-custom.php' );
require_once( get_template_directory() . '/classes/class-wp-text-custom.php' );
function wpse97413_register_custom_widgets() {
    register_widget( 'meinefahrzeug_Widget_Recent_Comments_Custom' );
    register_widget( 'meinefahrzeug_Widget_Recent_Posts_Custom' );
    register_widget( 'meinefahrzeug_Widget_Text' );
}
add_action( 'widgets_init', 'wpse97413_register_custom_widgets' );

function paginate_mein ($base_url, $query_str, $total_pages, 
                    $current_page, $paginate_limit)
{
    // Array to store page link list
    $page_array = array ();
    // Show dots flag - where to show dots?
    $dotshow = true;
    // walk through the list of pages
    for ( $i = 1; $i <= $total_pages; $i ++ )
    {
       // If first or last page or the page number falls 
       // within the pagination limit
       // generate the links for these pages
       if ($i == 1 || $i == $total_pages || 
             ($i >= $current_page - $paginate_limit && 
             $i <= $current_page + $paginate_limit) )
       {
          // reset the show dots flag
          $dotshow = true;
          // If it's the current page, leave out the link
          // otherwise set a URL field also
          if ($i != $current_page)
              $page_array[$i]['url'] = '#';
          $page_array[$i]['text'] = strval ($i);
       }
       // If ellipses dots are to be displayed
       // (page navigation skipped)
       else if ($dotshow == true)
       {
           // set it to false, so that more than one 
           // set of ellipses is not displayed
           $dotshow = false;
           $page_array[$i]['text'] = "...";
       }
    }
    // return the navigation array
    return $page_array;
}
//add_action( 'init', 'paginate_mein' );

/**
 * Enqueue scripts and styles.
 */
function mein_e_fahrzeug_scripts() {

	wp_enqueue_style( 'mein-e-fahrzeug-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-fonts', get_template_directory_uri() . '/fonts/fonts.min.css',false,'1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-bootstrap-select', get_template_directory_uri() . '/css/bootstrap-select.min.css',false,'1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-stars', get_template_directory_uri() . '/css/css-stars.css',false,'1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css',false,'1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css', false, '1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-owl-carousel-theme', get_template_directory_uri() . '/css/owl.theme.default.min.css', false, '1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-autocomplete', get_template_directory_uri() . '/css/easy-autocomplete.min.css', false, '1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-autocomplete-theme', get_template_directory_uri() . '/css/easy-autocomplete.themes.min.css', false, '1.1','all');
	wp_enqueue_style( 'mein-e-fahrzeug-style', get_stylesheet_uri() );


	wp_enqueue_script( 'mein-e-fahrzeug-jquery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-bootstrap-select-js', get_template_directory_uri() . '/js/bootstrap-select.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-barrating', get_template_directory_uri() . '/js/jquery.barrating.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-owl-carousel-js', get_template_directory_uri().'/js/owl.carousel.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-autocomplete-js', get_template_directory_uri().'/js/jquery.easy-autocomplete.min.js', array(), '20151215', true );
	wp_enqueue_script( 'mein-e-fahrzeug-custom', get_template_directory_uri() . '/js/custom.js', array(), '20151215', true );

	wp_localize_script( 'mein-e-fahrzeug-custom', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mein_e_fahrzeug_scripts' );

function custom_rewrite_tag() {
  add_rewrite_tag('%pagenum%', '([^&]+)');
  add_rewrite_tag('%vehicleid%', '([^&]+)');
  add_rewrite_tag('%place%', '([^&]+)');
  add_rewrite_tag('%pagex%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

function add_custom_query_var( $vars ){
  $vars[] = 'v';
  $vars[] = 't';
  return $vars;
}
add_filter( 'query_vars', 'add_custom_query_var' );

// define the custom replacement callback
function get_myname() {
    return 'My name is Moses';
}
function get_car_name() {
	global $wp_query;
	global $wpdb;
	$car = str_replace('--', '%', $wp_query->query['pagenum']);
	$car = str_replace('-', ' ', $car);
	if (substr_count($car, '%') > 1) {
		$car = str_replace('%%', ' - ', $car);
	}
	$car = str_replace('%', '-', $car);
	$myrows = $wpdb->get_row( "SELECT * FROM parser_evdatabase_vehicles WHERE car_name = '{$car}' LIMIT 0,1" );
	return ($myrows === null) ? '%%car_name%%' : $myrows->car_name;
}
function get_car_price() {
	global $wp_query;
	global $wpdb;
	$car = str_replace('--', '%', $wp_query->query['pagenum']);
	$car = str_replace('-', ' ', $car);
	if (substr_count($car, '%') > 1) {
		$car = str_replace('%%', ' - ', $car);
	}
	$car = str_replace('%', '-', $car);
	$myrows = $wpdb->get_row( "SELECT * FROM parser_evdatabase_vehicles WHERE car_name = '{$car}' LIMIT 0,1" );
	return ($myrows === null) ? '%%car_price%%' : number_format($myrows->car_price,0,",",".");
}
function get_car_kind() {
	global $wp_query;
	global $wpdb;
	$car = str_replace('--', '%', $wp_query->query['pagenum']);
	$car = str_replace('-', ' ', $car);
	if (substr_count($car, '%') > 1) {
		$car = str_replace('%%', ' - ', $car);
	}
	$car = str_replace('%', '-', $car);
	$car = ucwords($car);
	$myrows = $wpdb->get_row( "SELECT * FROM parser_evdatabase_vehicles WHERE car_name = '{$car}' LIMIT 0,1" );
	$kind = strstr($myrows->car_kind, 'Hybride') ? 'Plugin-Hybride' : 'Elektrisch';
	return ($myrows === null) ? '%%car_kind%%' : $kind;
}
function get_electric_range() {
	global $wp_query;
	global $wpdb;
	$car = str_replace('--', '%', $wp_query->query['pagenum']);
	$car = str_replace('-', ' ', $car);
	if (substr_count($car, '%') > 1) {
		$car = str_replace('%%', ' - ', $car);
	}
	$car = str_replace('%', '-', $car);
	$myrows = $wpdb->get_row( "SELECT * FROM parser_evdatabase_vehicles WHERE car_name = '{$car}' LIMIT 0,1" );
	return ($myrows === null) ? '%%car_name%%' : $myrows->electric_range;
}

function get_city_or_country() {
	global $wp_query;
	global $wpdb;
	$place = isset($wp_query->query['place']) ? urldecode($wp_query->query['place']) : null;
	$cities = $wpdb->get_row( "SELECT * FROM parser_location WHERE city_permalink = '{$place}'" );
	if ( $cities !== null ) {
		$name = $cities->city;
	}
	
	$country_param = str_replace('-', ' ', $place);
	$countries = $wpdb->get_row( "SELECT * FROM parser_countries WHERE country_name = '{$country_param}'" );
	if ( $countries !== null ) {
		$name = $countries->country_name;
	}
	return ($place === null) ? '' : 'in '.$name;
}

function get_network() {
	global $wp_query;
	$place = (isset($wp_query->query['place'])) ? urldecode($wp_query->query['place']) : null;
	return ($place === null) ? '' : 'von ' . $place;
}

function get_name() {
	global $wp_query;
	global $wpdb;

	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE `name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}'";

	$places = $wpdb->get_results($sql);
	$place = $places[0];
	return $place->name;
}
function get_street() {
	global $wp_query;
	global $wpdb;

	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE `name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}'";

	$places = $wpdb->get_results($sql);
	$place = $places[0];
	return $place->street;
}
function get_zip() {
	global $wp_query;
	global $wpdb;

	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE `name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}'";

	$places = $wpdb->get_results($sql);
	$place = $places[0];
	return $place->zip;
}
function get_city() {
	global $wp_query;
	global $wpdb;

	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE `name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}'";

	$places = $wpdb->get_results($sql);
	$place = $places[0];
	return $place->city;
}

function get_country_name() {
	global $wp_query;
	global $wpdb;

	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE `name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}'";

	$places = $wpdb->get_results($sql);
	$place = $places[0];
	return $place->country_name;
}

// define the action for register yoast_variable replacments
function register_custom_yoast_variables() {
    wpseo_register_var_replacement( '%%myname%%', 'get_myname', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%car_name%%', 'get_car_name', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%car_price%%', 'get_car_price', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%car_kind%%', 'get_car_kind', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%electric_range%%', 'get_electric_range', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%in_city_or_country%%', 'get_city_or_country', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%von_network%%', 'get_network', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%place_name%%', 'get_name', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%street%%', 'get_street', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%zip%%', 'get_zip', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%city%%', 'get_city', 'advanced', 'some help text' );
    wpseo_register_var_replacement( '%%country_name%%', 'get_country_name', 'advanced', 'some help text' );
}
// Add action
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');


add_filter('wpseo_title', 'filter_page_wpseo_title');
function filter_page_wpseo_title($title) {
	return $title;
}

add_action('admin_bar_menu', 'add_item', 100);

function add_item( $admin_bar ){
  global $pagenow;
  $admin_bar->add_menu( array( 'id'=>'cache-purge','title'=>'Generate Sitemap XML','href'=>'#' ) );
}

/* Here you trigger the ajax handler function using jQuery */

add_action( 'admin_footer', 'cache_purge_action_js' );

function cache_purge_action_js() { ?>
  <script type="text/javascript" >
     jQuery("li#wp-admin-bar-cache-purge .ab-item").on( "click", function() {
        var data = {
                      'action': 'example_cache_purge',
                    };

        /* since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php */
        jQuery.post(ajaxurl, data, function(response) {
           alert( response );
        });

      });
  </script> <?php
}

/* Here you hook and define ajax handler function */

add_action( 'wp_ajax_example_cache_purge', 'example_cache_purge_callback' );

function example_cache_purge_callback() {
    global $wpdb; /* this is how you get access to the database */
    /* You cache purge logic should go here. */

    //vehicles
    $path = get_template_directory().'/xml/elektroauto.xml';

    $xml = new DOMDocument('1.0', 'utf-8');
	$xml_urlset = $xml->createElement("urlset");
	$xml_urlset->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
	
	
	$myrows = $wpdb->get_results( "SELECT * FROM parser_evdatabase_vehicles" );
	foreach ($myrows as $row) {

		$xml_url = $xml->createElement("url");

		$slug = str_replace('-', '--', strtolower($row->car_name));
		$slug = str_replace(' ', '-', $slug);

		$xml_loc = $xml->createElement("loc");
		$urlencode = implode('/', array_map('rawurlencode', explode('/', $slug)));
		$urlencode = implode('+', array_map('rawurlencode', explode('+', $slug)));
		//$urlencode = implode('&', array_map('rawurlencode', explode('&', $slug)));
		$xml_loc->nodeValue = get_home_url() . '/elektroauto/' . $urlencode . '/';

		$xml_priority = $xml->createElement("priority");
		$xml_priority->nodeValue = 0.5;

		$xml_url->appendChild( $xml_loc );
		$xml_url->appendChild( $xml_priority );

		$xml_urlset->appendChild( $xml_url );
	}	
	$xml->appendChild( $xml_urlset );
	$xml->save($path);


	//–	e-stations per country → one sitemap per country. Each sitemap contains all e- stations for specific country.
    $path = get_template_directory().'/xml/';

    $countries = $wpdb->get_results( "SELECT * FROM parser_countries" );
    foreach ($countries as $country) {

    	$xml = new DOMDocument('1.0', 'utf-8');
		$xml_urlset = $xml->createElement("urlset");
		$xml_urlset->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
		
		$myrows = $wpdb->get_results( "SELECT * FROM parser_location WHERE country_id = '{$country->country_id}' GROUP BY name_permalink" );
		if (!empty($myrows)) {
			foreach ($myrows as $row) {

				$xml_url = $xml->createElement("url");


				$xml_loc = $xml->createElement("loc");
				$urlencode = implode('/', array_map('rawurlencode', explode('/', $row->name_permalink)));
				$urlencode = implode('+', array_map('rawurlencode', explode('+', $row->name_permalink)));
				//$urlencode = implode('&', array_map('rawurlencode', explode('&', $row->name_permalink)));
				$xml_loc->nodeValue = get_home_url() . '/stromtankstellen/' . $urlencode . '/';

				$xml_priority = $xml->createElement("priority");
				$xml_priority->nodeValue = 0.5;

				$xml_url->appendChild( $xml_loc );
				$xml_url->appendChild( $xml_priority );

				$xml_urlset->appendChild( $xml_url );
			}
			$xml->appendChild( $xml_urlset );
			$xml->save($path.'stromtankstellen_'.strtolower($country->country_name).'.xml');
		}
		
    }
    
    echo 'OK';
    wp_die(); /* this is required to terminate immediately and return a proper     response */
} 


function custom_rewrite_rule() {
    $elektroauto_single = get_page_by_path('elektroauto-single');
    add_rewrite_rule('^elektroauto/([^/]*)/?','index.php?page_id='.$elektroauto_single->ID.'&pagenum=$matches[1]','top');

    $stromtankstellen_list = get_page_by_path('stromtankstellen-list');
    add_rewrite_rule('^stromtankstellen-list/seite-([^/]*)/?','index.php?page_id='.$stromtankstellen_list->ID.'&pagex=$matches[1]','top');
    add_rewrite_rule('^stromtankstellen-list/([^/]*)/([^/]*)/seite-([^/]*)/?','index.php?page_id='.$stromtankstellen_list->ID.'&place=$matches[1]/$matches[2]&pagex=$matches[3]','top');
    add_rewrite_rule('^stromtankstellen-list/([^/]*)/seite-([^/]*)/?','index.php?page_id='.$stromtankstellen_list->ID.'&place=$matches[1]&pagex=$matches[2]','top');
    add_rewrite_rule('^stromtankstellen-list/([^/]*)/([^/]*)/?','index.php?page_id='.$stromtankstellen_list->ID.'&place=$matches[1]/$matches[2]&pagex=1','top');
    add_rewrite_rule('^stromtankstellen-list/([^/]*)/?','index.php?page_id='.$stromtankstellen_list->ID.'&place=$matches[1]&pagex=1','top');

    $stromtankstellen = get_page_by_path('stromtankstellen');
    add_rewrite_rule('^stromtankstellen/seite-([^/]*)/?','index.php?page_id='.$stromtankstellen->ID.'&pagex=$matches[1]','top');
    $stromtankstellen_single = get_page_by_path('stromtankstellen-single');
    add_rewrite_rule('^stromtankstellen/([^/]*)/seite-([^/]*)/?','index.php?page_id='.$stromtankstellen_single->ID.'&place=$matches[1]&pagex=$matches[2]','top');
    add_rewrite_rule('^stromtankstellen/([^/]*)/([^/]*)/([^/]*)/?','index.php?page_id='.$stromtankstellen_single->ID.'&place=$matches[1]/$matches[2]/$matches[3]&pagex=1','top');
    add_rewrite_rule('^stromtankstellen/([^/]*)/([^/]*)/?','index.php?page_id='.$stromtankstellen_single->ID.'&place=$matches[1]/$matches[2]&pagex=1','top');
    add_rewrite_rule('^stromtankstellen/([^/]*)/?','index.php?page_id='.$stromtankstellen_single->ID.'&place=$matches[1]&pagex=1','top');
    
    $verbund_list = get_page_by_path('verbund');
    add_rewrite_rule('^verbund/seite-([^/]*)/?','index.php?page_id='.$verbund_list->ID.'&place=&pagex=$matches[1]','top');
    add_rewrite_rule('^verbund/([^/]*)/([^/]*)/seite-([^/]*)/?','index.php?page_id='.$verbund_list->ID.'&place=$matches[1]/$matches[2]&pagex=$matches[3]','top');
    add_rewrite_rule('^verbund/([^/]*)/seite-([^/]*)/?','index.php?page_id='.$verbund_list->ID.'&place=$matches[1]&pagex=$matches[2]','top');
    add_rewrite_rule('^verbund/([^/]*)/([^/]*)/?','index.php?page_id='.$verbund_list->ID.'&place=$matches[1]/$matches[2]&pagex=1','top');
    add_rewrite_rule('^verbund/([^/]*)/?','index.php?page_id='.$verbund_list->ID.'&place=$matches[1]&pagex=1','top');
	
}
add_action('init', 'custom_rewrite_rule', 10, 0);

add_filter('comment_post_redirect', 'redirect_after_comment');
function redirect_after_comment($location)
{
	return $_SERVER['HTTP_REFERER'];
}
add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_prev');

function posts_link_attributes_prev() {
	return 'class="prev-post"';
}

function posts_link_attributes_next() {
	return 'class="next-post"';
}

// And save the comment meta
add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
	add_comment_meta( $comment_id, 'vote-type', $_POST[ 'vote-type' ] );
}

// Retrieve and display comment meta
add_filter( 'get_comment_author_link', 'attach_vote_type_to_author' );
function attach_vote_type_to_author( $author ) {
	$vote_type = get_comment_meta( get_comment_ID(), 'vote-type', true );
	if ( $vote_type )
		$author .= " ($vote_type)";
	return $author;
}


function filter_pre_comment_content( $array ) { 
	return wp_strip_all_tags($array);
}
add_filter( 'pre_comment_content', 'filter_pre_comment_content' );

function add_custom_comment_field( $comment_id ) {
	add_comment_meta( $comment_id, 'rating', $_POST['rating'] );
	add_comment_meta( $comment_id, 'context', $_POST['context'] );
	add_comment_meta( $comment_id, 'context_id', $_POST['context_id'] );
}
add_action( 'comment_post', 'add_custom_comment_field' );


function get_vehicle_overview() {
	require_once( get_template_directory() . '/template-parts/ajax-vehicle-overview.php' );
}
add_action('wp_ajax_nopriv_get_vehicle_overview', 'get_vehicle_overview');
add_action('wp_ajax_get_vehicle_overview', 'get_vehicle_overview');


function get_location() {
	require_once( get_template_directory() . '/template-parts/ajax-location.php' );
}
add_action('wp_ajax_nopriv_get_location', 'get_location');
add_action('wp_ajax_get_location', 'get_location');

function get_location_list() {
	require_once( get_template_directory() . '/template-parts/ajax-stromtankstellen.php' );
}
add_action('wp_ajax_nopriv_get_location_list', 'get_location_list');
add_action('wp_ajax_get_location_list', 'get_location_list');

add_filter( 'comments_template_query_args', function( $comment_args )
{
    // Only modify non-threaded comments
    if(    isset( $comment_args['hierarchical'] ) 
        && 'threaded' === $comment_args['hierarchical'] 
    )
        return $comment_args;

    // Our modifications
    return $comment_args;
} );

add_action( 'add_meta_boxes_comment', 'comment_add_meta_box' );
function comment_add_meta_box()
{
 add_meta_box( 'my-comment-title', 'Additional Info', 'comment_meta_box_age',     'comment', 'normal', 'high' );
}

function comment_meta_box_age( $comment )
{
	global $wpdb;
    $title = get_comment_meta( $comment->comment_ID, 'rating', true );
    $ct_type = get_comment_meta( $comment->comment_ID, 'context', true );
    $ct_id = get_comment_meta( $comment->comment_ID, 'context_id', true );

    if ($ct_type == 'vehicle') {
    	$row = $wpdb->get_row(" SELECT * FROM parser_evdatabase_vehicles WHERE vehicle_id = '{$ct_id}' ");
    	$delimiter = '-';
    	$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $row->car_name))))), $delimiter));
    	$link = '<a href="' . get_home_url() . '/elektroauto/' . $row->vehicle_id . '/' . $slug . '/#comment-' . $comment->comment_ID . '">' . $row->car_name . '</a>';
    } else if ($ct_type == 'place') {
    	$row = $wpdb->get_row(" SELECT * FROM parser_location WHERE place_id = '{$ct_id}' ");
    	$slug = $row->name_permalink;
    	$link = '<a href="' . get_home_url() . '/stromtankstellen/' . $slug . '/#comment-' . $comment->comment_ID . '">' . $row->name . '</a>';
    } else {
    	$link = '';
    }

    
   ?>
 <p>
     <label for="rating">Rating Given</label>;
     <input type="text" name="rating" value="<?php echo esc_attr( $title ); ?>"  class="widefat" />
 </p>
 <?php if ($link != '') : ?>
 <p>
 	<label for="">Link</label>
 	<div>
 		<?php echo $link; ?>
 	</div>
 </p>
<?php endif; ?>
 <?php
}
add_action( 'edit_comment', 'comment_edit_function' );
function comment_edit_function( $comment_id )
{
    if( isset( $_POST['age'] ) )
      update_comment_meta( $comment_id, 'age', esc_attr( $_POST['age'] ) );
}

add_filter('the_title', 'filter_pagetitle');
function filter_pagetitle($title) {
    //check if its a blog post
    if (!is_single())
        return $title;

    //if you get here then its a blog post so change the title
    global $wp_query;
    if (isset($wp_query->post->post_title)){
        return $wp_query->post->post_title;
    }

    //if wordpress can't find the title return the default
    return $title;
}

add_filter( 'wpseo_canonical', '__return_false' );
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

