<?php
	global $wpdb;

	$page_num = isset($_POST['pagenum']) ? $_POST['pagenum'] : 1;
	$offset = ($page_num == 1) ? 0 : 10 * ($page_num - 1);
	//country = 1, city = 2
	$permalink = $_POST['permalink'];
	if ($permalink == '') {
		//list all
		$param = '';
	} else {
		if ($_POST['target'] == '1') {
			$country = $wpdb->get_results( "SELECT * FROM parser_countries WHERE country_name = '{$permalink}'" );
			$headline = $country[0]->country_name;
			$param = "WHERE country_id = '{$country[0]->country_id}'";
		} else if ($_POST['target'] == '2') {
			$param = "WHERE city_permalink = '{$permalink}'";
			$city = $wpdb->get_results( "SELECT * FROM parser_location WHERE city_permalink = '{$permalink}' LIMIT 1" );
			$headline = $city[0]->city;
		} 
	}

	if ($_POST['target'] == '1' || $_POST['target'] == '2') {
		$total_list = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_location " . $param );
		$pages = ceil($total_list[0]->total / 100);
		$locations = $wpdb->get_results( "SELECT * FROM parser_location " . $param . " LIMIT {$offset},100");
		$link_base = 'stromtankstellen';
	} else {
		if ($permalink != '') {
			$permalink = str_replace('---', ' / ', $permalink);
			$permalink = str_replace('--', '..', $permalink);
			$permalink = str_replace('-', ' ', $permalink);
			$permalink = str_replace('..', '-', $permalink);
			$permalink = ucwords($permalink);
			$total_list = $wpdb->get_results( " SELECT COUNT(*) AS total FROM parser_location INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id WHERE parser_station.network = '{$permalink}'" );
			$pages = ceil($total_list[0]->total / 100);
			$locations = $wpdb->get_results( "SELECT * FROM parser_location INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id WHERE parser_station.network = '{$permalink}' LIMIT {$offset},100");
			$link_base = 'stromtankstellen';
		} else {
			$total_list = $wpdb->get_results( " SELECT COUNT(DISTINCT(network)) AS total FROM parser_station WHERE network <> ''" );
			$pages = ceil($total_list[0]->total / 100);
			$stations = $wpdb->get_results("SELECT DISTINCT(network) FROM parser_station WHERE network <> '' ORDER BY network ASC LIMIT {$offset},100");
			$locations = array();
			$delimiter = '-';
			foreach ($stations as $s) {
				$slug = str_replace('-', '--', $s->network);
				$slug = str_replace(' ', '-', $slug);
				$slug = str_replace('/', '-', $slug);
				$row = new StdClass();
				$row->name_permalink = $slug;
				$row->name = $s->network;
				$locations[] = $row;
			}
			$link_base = 'verbund';
		}
		
	}
	
?>
<h1>Alle Ladestationen <?php echo ($permalink == '' || $_POST['target'] == '3') ? '' : 'in';?> <?php echo $headline; ?></h1>
<?php if (count($locations) > 0) :?>
	<ul class="list-unstyled e-station-list">
		<?php foreach ($locations as $loc) :?>
		<li><a href="<?php echo get_home_url(); ?>/<?php echo $link_base; ?>/<?php echo htmlspecialchars($loc->name_permalink);?>/"><?php echo $loc->name; ?></a></li>
		<?php endforeach; ?>
	</ul>

	<div class="text-center e-station-list-page">
		<ul class="list-unstyled pagination">
			<?php if ($page_num > 1) :?>
			<li class="first"><a href="#" class="page" data-page="1"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
			<li class="prev"><a href="#" class="page" data-page="<?php echo $page_num - 1;?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
			<?php endif; ?>
			<?php 
			$pages_ellipsis = paginate_mein("#", "", $pages, $page_num, $radius = 2);
			// list display
			foreach ($pages_ellipsis as $page) {
			    // If page has a link
			    if (isset ($page['url'])) { ?>
			        <li><a href="<?php echo $page['url']?>" class="page" data-page="<?php echo $page['text'] ?>">
					<?php echo $page['text'] ?>
				</a></li>
			<?php }
			    // no link - just display the text
			     else 
			     	if ($page['text'] == $page_num) {
			     		echo '<li class="active"><a href="#">'.$page['text'].'</a></li>';
			     	} else {
			     		echo '<li><a href="#" class="ellipsis">'.$page['text'].'</a></li>';
			     	}
			        
			} ?>
			<?php if ($page_num < $pages) : ?>
				<li class="next"><a href="#" class="page" data-page="<?php echo $page_num + 1; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li class="last"><a href="#" class="page" data-page="<?php echo $pages; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
			<?php endif; ?>
		</ul>
	</div>

<?php endif; ?>

<?php exit(); ?>