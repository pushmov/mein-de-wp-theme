<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mein-e-fahrzeug
 */
global $wp;
$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

?>
<div class="box box-shadow news-detail">
	<div class="share">
		<ul class="list-unstyled">
			<li><a class="tw" data-url="<?php echo home_url( $wp->request ); ?>" href="#" data-text="<?php echo the_title();?>" data-url="<?php echo home_url( $wp->request ); ?>"><i class="fa fa-twitter fa-2x"></i></a></li>
			<li><a class="fb" data-url="<?php echo home_url( $wp->request ); ?>" data-title="<?php echo the_title();?>" data-img="<?php echo $img[0]; ?>" data-description="<?php echo limit_text(get_the_content(), 50); ?>" href="#"><i class="fa fa-facebook fa-2x"></i></a></li>
		</ul>
	</div>
	<div class="text-right entry-meta">
		<em><?php echo date('d.m.Y', strtotime(get_the_date())); ?></em>
	</div><!-- .entry-meta -->

	<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
	?>
				
	<?php endif; ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		

		<div class="entry-content">
			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'mein-e-fahrzeug' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mein-e-fahrzeug' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

	</article><!-- #post-<?php the_ID(); ?> -->
</div>