<div class="row" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="news-box box-shadow clearfix">
		<?php
		if ( is_singular() ) :
			the_title( '<h3 class="entry-title">', '</h3>' );
		else :
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		endif;
		?>
		<div class="row">
			<div class="col-md-4">
				<?php mein_e_fahrzeug_post_thumbnail(); ?>
			</div>
			<div class="col-md-8">
				<p><?php echo wp_strip_all_tags(limit_text(get_the_content(), 50));?></p>
				<a href="<?php echo esc_url( get_permalink() ); ?>">» mehr</a>
			</div>
		</div>
	</div>
</div>