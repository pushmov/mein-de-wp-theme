<?php 
global $wp_query;
global $wpdb;
$myrows = $wpdb->get_results("SELECT place_id, name, name_permalink, city, city_permalink, parser_countries.country_id, country_name FROM parser_location INNER JOIN parser_countries ON parser_countries.country_id = parser_location.country_id GROUP BY city ORDER BY city ASC");
$data = array();
foreach ($myrows as $row) {
	$data[] = array('name' => $row->city, 'code' => $row->city_permalink);
}
echo json_encode($data);
exit();
?>