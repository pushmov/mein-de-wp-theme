<?php
	$param = (urldecode($wp_query->query['place']));
	$param = htmlentities($param);
	$param = stripslashes($param);
	$param = str_replace("'","", html_entity_decode($param, ENT_QUOTES));
	$param = str_replace(' ', '+', $param);
	$spec_param = htmlspecialchars($param);


	$subsql = '(SELECT *, (REPLACE( `name_permalink`, "\'", "" )) AS `custom` FROM parser_location ) AS tmp';
	$sql = "SELECT * FROM ".$subsql." INNER JOIN parser_countries ON parser_countries.country_id = tmp.country_id INNER JOIN parser_station ON tmp.place_id = parser_station.place_id WHERE (`name_permalink` LIKE '{$param}' OR custom LIKE '{$param}' OR `name_permalink` LIKE '{$spec_param}' OR custom LIKE '{$spec_param}') AND parser_station.status = 'ok'";

	global $placex;
	$placex = $place = $wpdb->get_row($sql);

	if (empty($place)) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}

	$addr_placeholder = $place->street . ', '.$place->city.', '.$place->zip;
	$address = $place->street . ', '.$place->city.', '.$place->zip . ', '.$place->country_name;

	get_header('stromtankstellen-detail');

	$custom = get_post_custom();
	$key = isset($custom['GOOGLE_MAP_API_KEY']) ? $custom['GOOGLE_MAP_API_KEY'][0] : 'AIzaSyC_q1HC1jtop7F_kFs7vt6Nl2WWdpvUEac';

	define("MAPS_HOST", "maps.google.com");
	define("KEY", $key);	//Personal Google Maps API key
	if( $address!=NULL)
	{

		$geocode = $wpdb->get_row("SELECT * FROM parser_location_geocode WHERE place_id = '{$place->place_id}'");

		if ($geocode === null) {
			// Initialize delay in geocode speed
			$delay = 0;
			$base_url = "https://" . MAPS_HOST . "/maps/geo?output=xml" . "&key=" . KEY;

			$request_url = "https://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=false&key=".KEY;
			$xml = simplexml_load_file($request_url) or die("URL not loading");
			$status = $xml->status;
		    if (strcmp($status, "OK") == 0)
			{
		      // Successful geocode
		      $geocode_pending = false;
		      // Format: Longitude, Latitude, Altitude
		      $lat = $xml->result->geometry->location->lat;
		      $lng = $xml->result->geometry->location->lng;

		      $insert = $wpdb->insert('parser_location_geocode', array(
		      	'place_id' => (int) $place->place_id,
		      	'latitude' => (float) $lat,
		      	'longitude' => (float) $lng
		      ));

		/*****************************************************************************************************/
				//Showing the image
			  $mapimage = "https://maps.googleapis.com/maps/api/staticmap?markers=color:red|".$lat.",".$lng."&size=640x150&maptype=roadmap&zoom=13&scale=2&key=".KEY;

			}
			else if (strcmp($status, "620") == 0)
			{
		      // sent geocodes too fast
		      $delay += 100000;
		    }
			else {
		      // failure to geocode
		      $geocode_pending = false;
		      echo "Address " . $address . " failed to be Geocoded.<br>Received status " . $status . "<br><br>Please check your address";
		    }
			usleep($delay);
		} else {
			$lat = $geocode->latitude;
			$lng = $geocode->longitude;
			$mapimage = "https://maps.googleapis.com/maps/api/staticmap?markers=color:red|".$lat.",".$lng."&size=640x150&maptype=roadmap&zoom=13&scale=2&key=".KEY;
		}

	}

?>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo KEY; ?>"></script>

<section class="ct-box"></section>
<section class="ct-map-wrapper">
	<div class="container">
		<div id="map_layer" class="map-layer" style="display: none">
			<div class="control-panel">
				<div class="pull-right"><a href="#" id="close_map">Karte schließen <i class="fa fa-times-rectangle"></i></a></div>
				<form class="form-inline">
					<h3>Ihre Route zu <?php echo $place->name?>:</h3>
					<div class="col-sm-3">
						<div class="form-group transportation">
							<button type="button" class="btn btn-default" id="DRIVING"><i class="fa fa-car"></i></button>
							<button type="button" class="btn btn-default" id="BICYCLING"><i class="fa fa-bicycle"></i></button>
	    					<button type="button" class="btn btn-default" id="TRANSIT"><i class="fa fa-bus"></i></button>
	    					<button type="button" class="btn btn-default" id="WALKING"><i class="fa fa-motorcycle"></i></button>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="form-group">
	    					<label for="start_address">Start:</label>
	    					<input type="text" class="form-control" id="start_address" placeholder="Straße, Ort" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?">
	  					</div>
	  					<div class="form-group">
	    					<label for="dest_address">Ziel:</label>
	    					<input type="text" class="form-control" id="dest_address" placeholder="" value="<?php echo $addr_placeholder; ?>">
	  					</div>
  						<button type="submit" class="btn btn-default" id="show_route">Route berechnen</button>
  					</div>
				</form>
			</div>

			<div id="map"></div>
		</div>
		<a target="_blank" class="map-container" href="<?php echo $mapimage; ?>">
			<img src="<?php echo $mapimage; ?>" height="250" border="0" width="100%">
		</a>
		<style type="text/css">
			a.map-container{
				background-image: url("<?php echo $mapimage; ?>");
			}
		</style>
	</div>
</section>
<section class="ct-stromtankstellen e-station-detail">

	<div class="container">
		<button type="button" id="map_reveal" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" class="pull-right map-reveal"><i class="fa fa-caret-down"></i>komplette Karte anzeigen</button>

		<div class="breadcrumb-nav">

			<div class="clearfix">
				<div class="col-md-12">
					<div class="nav">
						<ol class="list-unstyled e-station-detail-list" itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing"
									itemprop="item" href="<?php echo get_home_url(); ?>/">
									<i class="fa fa-home fa-2x"></i>
									<span class="hide" itemprop="name">Home</span>
								</a>
								<meta itemprop="position" content="1" />
							</li>
							<li itemprop="itemListElement" itemscope
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing"
									itemprop="item" href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo strtolower(str_replace(' ', '-', htmlspecialchars($place->country_name)));?>/">
									<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $place->country_name; ?></span></strong>
								</a>
								<meta itemprop="position" content="2" />
							</li>
							<li itemprop="itemListElement" itemscope
								itemtype="http://schema.org/ListItem">
								<a itemscope itemtype="http://schema.org/Thing"
									itemprop="item" href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $place->city_permalink; ?>/">
									<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $place->city;?></span></strong>
								</a>
								<meta itemprop="position" content="3" />
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-9">
				<div class="box box-info">
					<div class="row">
						<div class="col-md-5">
							<div class="box-stromtankstellen-left">
								<?php get_sidebar('stromtankstellen-ads');?>
							</div>
						</div>
						<div class="col-md-7">
							<div class="box-stromtankstellen-right">
								<h1><?php echo $place->name; ?></h1>
								<address class="icon-location">
									<?php echo $place->street;?> <br>
									<?php echo $place->zip; ?> <?php echo $place->city;?> <a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $place->city_permalink; ?>/"><i class="fa fa-external-link" aria-hidden="true"></i></a><br>
									<?php echo $place->country_name; ?> <a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo strtolower(str_replace(' ', '-', $place->country_name)); ?>/"><i class="fa fa-external-link" aria-hidden="true"></i></a>
								</address>
								<ul class="list-unstyled e-station-detail-list">
									<?php if ($place->website != '') :?>
									<li class="icon-globe">zur Homepage <a href="<?php echo $place->website; ?>" rel="nofollow"><i class="fa fa-external-link" aria-hidden="true"></i></a></li>
									<?php endif; ?>

									<?php if ($place->opening_hours != '') : ?>
									<li class="icon-clock"><?php echo $place->opening_hours; ?></li>
									<?php endif; ?>

									<?php if ($place->parking != '') :?>
									<li class="icon-park"><?php echo $place->parking; ?></li>
									<?php endif; ?>
									<li class="icon-plug">Ladestecker: <?php echo $place->plug_amount; ?> Stück
										<?php if (count(unserialize($place->plugs)) > 0) :?>
										<ul class="list-unstyled e-station-detail-list">
											<?php foreach (unserialize($place->plugs) as $plug) : ?>
												<?php $plugs_ct = $wpdb->get_results("SELECT * FROM parser_plug WHERE plug_id = '{$plug}'");?>
											<li class="icon-kw"><div class="numberCircle"><?php echo $plugs_ct[0]->plug_amount; ?></div><?php echo $plugs_ct[0]->plug_value;?></li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<h3><i class="fa fa-caret-right"></i>Position/Lage</h3>
				<div class="box box-shadow">
					<?php if ($place->position == '') : ?>
					<p>keine Informationen zur Lage der Ladesäule bekannt</p>
					<?php else : ?>
					<p><?php echo $place->position; ?></p>
					<?php endif; ?>
				</div>

				<h3><i class="fa fa-caret-right"></i>Ladekarten/Angebote</h3>
				<div class="box box-shadow">

					<?php
						if ($place->cards != 'N;') :
						$cards = unserialize($place->cards);
					?>
					<ul>
						<?php foreach ($cards as $card) :
						$cards_ct = $wpdb->get_results("SELECT * FROM parser_cards WHERE card_id = '{$card}'");
						?>
						<li><?php echo $cards_ct[0]->card_value; ?></li>
						<?php endforeach; ?>
					</ul>
					<?php else : ?>
					<p>Keine Informationen zu den Ladekarten vorhanden</p>
					<?php endif; ?>
				</div>

				<h3><i class="fa fa-caret-right"></i>Kosten</h3>
				<div class="box box-shadow">
					<?php if ($place->costs != '') : ?>
					<?php echo $place->costs; ?>
					<?php else : ?>
					<p>Keine Informationen zur Höhe der Kosten bekannt</p>
					<?php endif; ?>
				</div>

				<h3><i class="fa fa-caret-right"></i>Betreiber</h3>
				<div class="box box-shadow">
					<div class="row">
						<div class="col-md-12">
							<ul class="list-unstyled e-station-detail-list">
								<li><span>Betreiber:</span> <?php echo $place->owner; ?></li>
								<?php $delimiter = '-';
									//$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $place->network))))), $delimiter));
									//$slug = str_replace('---', '/', $slug);
									//$slug = str_replace('-', '--', $place->network);
									//$slug = str_replace(' ', '-', $slug);
									//$slug = str_replace('/', '-', $slug);

								?>
								<li><span>Verbund:</span> <a href="<?php echo get_home_url(); ?>/verbund/<?php echo implode('/', array_map('rawurlencode', explode('/', $place->network))); ?>/"><?php echo $place->network; ?></a></li>
							</ul>
						</div>
					</div>
				</div>

				<h3><i class="fa fa-caret-right"></i>User über <?php echo $place->name; ?></h3>
				<div class="box box-shadow">
					<div class="comment-list">
					<?php
					$stromtankstellen_single = get_page_by_path('stromtankstellen-single');
					$comment_args = apply_filters( 'comments_template_query_args', array(
						'post_id' => $stromtankstellen_single->ID,
						'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'context',
								'value' => 'place',
								'compare' => '='
							),
							array(
								'key' => 'context_id',
								'value' => $place->place_id,
								'compare' => '='
							)
						)
					) );
					$comment_query = new WP_Comment_Query;
					$comments = $comment_query->query($comment_args);

					$total = 0;
					$total_rating = 0;
					if ($comments) {

						foreach ($comments as $comment) :
							$rating_meta = get_comment_meta($comment->comment_ID, 'rating');
							$rating = isset($rating_meta[0]) ? $rating_meta[0] : 0;

							$context_meta = get_comment_meta($comment->comment_ID, 'context');
							$context_id = get_comment_meta($comment->comment_ID, 'context_id');

							$width = $rating * 20;
							$total_rating += $rating;

							?>
							<div class="comment-item" id="comment-<?php echo $comment->comment_ID; ?>">
								<div class="comment-meta post-meta" role="complementary">
									<?php if ($comment->comment_approved != '0') : ?>
									<div class="row">
										<div class="col-sm-3">
											<div class="rating-wrapper">
												<div class="ratings">
							                        <div class="empty-stars"></div>
							                        <div class="full-stars" data-value="<?php echo $rating; ?>" style="width:<?php echo $width;?>%"></div>
							                    </div>
							                </div>
										</div>
										<div class="col-sm-9">
											<?php
												$month = array(
													'01' => 'Januar',
													'02' => 'Februar',
													'03' => 'März',
													'04' => 'April',
													'05' => 'Mai',
													'06' => 'Juni',
													'07' => 'Juli',
													'08' => 'August',
													'09' => 'September',
													'10' => 'Oktober',
													'11' => 'November',
													'12' => 'Dezember'
												);
												$gdate[] = date('d.', strtotime($comment->comment_date));
												$gdate[] = $month[date('m', strtotime($comment->comment_date))];
												$gdate[] = date('Y', strtotime($comment->comment_date));

												$gtime[] = date('H:i', strtotime($comment->comment_date));
												$gtime[] = 'Uhr';
											?>
											<h2 class="comment-author"><em><?php comment_author(); ?> am
												<time class="comment-meta-item" datetime="<?php comment_date('Y-m-d') ?>T<?php comment_time('H:iP') ?>" itemprop="datePublished"><?php echo join(' ', $gdate); ?>, <?php echo join(' ', $gtime); ?></time>
											</em></h2>
										</div>
									</div>
									<?php endif; ?>

									<?php //edit_comment_link('<p class="comment-meta-item">Edit this comment</p>','',''); ?>
									<?php if ($comment->comment_approved == '0') : ?>
									<p class="comment-meta-item">Your comment is awaiting moderation.</p>
									<?php endif; ?>
								</div>
								<div class="comment-content post-content" itemprop="text">
									<?php if ($comment->comment_approved != '0') : ?>
									<?php comment_text() ?>
									<?php endif; ?>
								</div>
							</div>
							<?php
						$total++;endforeach;
					}

					?>
					</div>
					<?php comments_template(); ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="rating-box">
					<?php
						$overral = ($total == 0) ? 0 : ($total_rating / $total);
					?>
					<h4>Anzahl der Bewertungen: <?php echo $total; ?></h4>
					<h4>Durchschnittliche Bewertung:</h4>
					<div class="input-group">
      					<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
      						<meta content="<?php echo sprintf("%0.1f",$overral);?>" itemprop="ratingValue">
      						<div style="display:none">
	                            <span itemprop="itemReviewed"><?php echo $place->name; ?></span>
	                            <span itemprop="bestRating">5</span>
	                            <span itemprop="worstRating">0</span>
	                            <span itemprop="reviewCount"><?php echo $total; ?></span>
	                        </div>
      						<div class="ratings">
								<div class="empty-stars"></div>
								<div class="full-stars" data-value="<?php echo $rating; ?>" style="width:<?php echo ($overral * 20);?>%"></div>
							</div>
      					</div>
      					<div class="input-group-addon"><?php echo sprintf("%0.1f",$overral);?></div>
    				</div>
				</div>

				<div class="aktualisierung">
					<h3><i class="fa fa-caret-right"></i>Letzte Aktualisierung</h3>
					<div class="box box-shadow text-center">
						<p>am 26.03.2018</p>
						<a href="<?php echo get_home_url(); ?>/contact/" class="btn btn-primary btn-e-auto-dropdown contact-href">Datenänderung mitteilen<i class="fa fa-caret-right"></i></a>
					</div>
				</div>
				<?php get_sidebar('place'); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
<script type="text/javascript">
	jQuery('#context').val('place');
	jQuery('#context_id').val('<?php echo $place->place_id; ?>');
</script>
