<?php
	global $wpdb;
	define('MAX_ITEM', 100);

	if ($is_country) {
		$country = $wpdb->get_results( "SELECT * FROM parser_countries WHERE country_name = '{$country_param}'" );
		$headline = $country[0]->country_name;
		$param = "WHERE country_id = '{$country[0]->country_id}' AND status = 'ok'";
		$permalink = $country_param;
	} else {
		$param = "WHERE city_permalink = '{$place}' AND status = 'ok'";
		$city = $wpdb->get_results( "SELECT * FROM parser_location WHERE city_permalink = '{$place}' LIMIT 1" );
		$headline = $city[0]->city;
		$permalink = $place;
	}

	$page_num = isset($wp_query->query['pagex']) ? $wp_query->query['pagex'] : 1;

	//country = 1, city = 2
	$target = ($is_country) ? '1' : '2';
	$locations = $wpdb->get_results( "SELECT parser_location.*, parser_station.status FROM parser_location INNER JOIN parser_station ON parser_location.place_id = parser_station.place_id " . $param . " LIMIT 0," . MAX_ITEM);
	$offset = ($page_num == 1 || $page_num === null) ? 0 : MAX_ITEM * ($page_num - 1);

	if ($permalink == '') {
		//list all
		$param = '';
	} else {
		if ($target == '1') {
			$country = $wpdb->get_results( "SELECT * FROM parser_countries WHERE country_name = '{$permalink}'" );
			$headline = $country[0]->country_name;
			$param = "WHERE country_id = '{$country[0]->country_id}'";
		} else if ($target == '2') {
			$param = "WHERE city_permalink = '{$permalink}' AND status = 'ok'";
			$city = $wpdb->get_results( "SELECT * FROM parser_location WHERE city_permalink = '{$permalink}' LIMIT 1" );
			$headline = $city[0]->city;
		}
	}

	if ($target == '1' || $target == '2') {
		$total_list = $wpdb->get_results( "SELECT COUNT(DISTINCT name_permalink) AS total FROM parser_location INNER JOIN parser_station ON parser_location.place_id = parser_station.place_id " . $param );
		$pages = ceil($total_list[0]->total / MAX_ITEM);
		$max = $pages;
		$locations = $wpdb->get_results( "SELECT parser_location.*,parser_station.status FROM parser_location INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id " . $param . " GROUP BY name_permalink LIMIT {$offset}," . MAX_ITEM);
		$link_base = 'stromtankstellen';
	} else {
		if ($permalink != '') {
			$permalink = str_replace('---', ' / ', $permalink);
			$permalink = str_replace('--', '..', $permalink);
			$permalink = str_replace('-', ' ', $permalink);
			$permalink = str_replace('..', '-', $permalink);
			$permalink = ucwords($permalink);
			$total_list = $wpdb->get_results( " SELECT COUNT(*) AS total FROM parser_location INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id WHERE parser_station.network = '{$permalink}'" );
			$pages = ceil($total_list[0]->total / MAX_ITEM);
			$max = $pages;
			$locations = $wpdb->get_results( "SELECT * FROM parser_location INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id WHERE parser_station.network = '{$permalink}' LIMIT {$offset}," . MAX_ITEM);
			$link_base = 'stromtankstellen';
		} else {
			$total_list = $wpdb->get_results( " SELECT COUNT(DISTINCT(network)) AS total FROM parser_station WHERE network <> '' AND status = 'ok'" );
			$pages = ceil($total_list[0]->total / MAX_ITEM);
			$max = $pages;
			$stations = $wpdb->get_results("SELECT DISTINCT(network) FROM parser_station WHERE network <> '' AND status = 'ok' ORDER BY network ASC LIMIT {$offset}," . MAX_ITEM);
			$locations = array();
			$delimiter = '-';
			foreach ($stations as $s) {
				$slug = str_replace('-', '--', $s->network);
				$slug = str_replace(' ', '-', $slug);
				$slug = str_replace('/', '-', $slug);
				$row = new StdClass();
				$row->name_permalink = $slug;
				$row->name = $s->network;
				$locations[] = $row;
			}
			$link_base = 'verbund';
		}
	}

	$pages_ellipsis = paginate_mein("#", "", $pages, $page_num, $radius = 2);
	$permalink_url = ($permalink == '') ? '' : $permalink.'/';

	get_header('stromtankstellen');
	$custom = get_post_custom();
	$key = isset($custom['GOOGLE_MAP_API_KEY']) ? $custom['GOOGLE_MAP_API_KEY'][0] : 'AIzaSyCvnCXooa_k7AcoBv3khndkRguR9TwbRqk';

	define("MAPS_HOST", "maps.google.com");
	define("KEY", $key);

	//list all markers position
	$markers = array();
	if (count($locations) > 0) {
		foreach ($locations as $marker) {
			$position = $wpdb->get_row("SELECT * FROM parser_location_geocode WHERE place_id = '{$marker->place_id}'");
			if ($position) {
				$markers[] = array($marker->name, $position->latitude, $position->longitude, $position->place_id, $marker->street, $marker->zip, $marker->website, $marker->name_permalink);
			} else {
				//google map lat,lng request?
			}
		}
	}

	$find_max_plug = "SELECT parser_location.place_id,parser_station.plugs FROM `parser_location` INNER JOIN parser_station ON parser_station.place_id = parser_location.place_id WHERE city_permalink = '{$permalink}' ORDER BY LENGTH(plugs) DESC LIMIT 0,1";
	$query_plug = $wpdb->get_row($find_max_plug);
	$max_plug = ($permalink != '' && $target == '2') ? count(unserialize($query_plug->plugs)) : 1;


	for ( $i=1; $i<=$max_plug; $i++) {
		$sql_select_alias[] = "plug_id_" . $i;
		$sql_select_alias[] = "p" . $i . ".plug_value AS name_" . $i;
		$sql_select_alias[] = "p" . $i . ".plug_amount AS amt_" . $i;

		$sql_condition[] = "if (LOCATE(';}', plug_id_" . $i . ") > 0, NULL, plug_id_" . $i . ") AS plug_id_" . $i;

		$sql_trim[] = "TRIM(BOTH '\"' FROM plug_id_" . $i . ") AS plug_id_" . $i;

		$sql_substring[] = "SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';'," . ($i * 2) . "), ':', -1) AS plug_id_" . $i;
		$sql_join[] = "LEFT JOIN parser_plug AS p".$i." ON p".$i.".plug_id = plug_tmp.plug_id_" . $i;
	}

	$sql_select_alias = join(',', $sql_select_alias);
	$sql_condition = join(',', $sql_condition);
	$sql_trim = join(',', $sql_trim);
	$sql_substring = join(',', $sql_substring);
	$sql_join = join(' ', $sql_join);

	$stat_counter = "SELECT
				place_id,
				name_permalink,
				plug_amount_total AS plug_amt,
				{$sql_select_alias}
			FROM
				(
					SELECT
						place_id,
						plug_amount_total,
						name_permalink,
						{$sql_condition}
					FROM (
						SELECT
							place_id,
							name_permalink,
							plug_amount_total,
							{$sql_trim}
							FROM
								(SELECT
									location_tmp.place_id,
									location_tmp.name_permalink,
									parser_station.plug_amount AS plug_amount_total,
									{$sql_substring}
								FROM
									(SELECT place_id,name_permalink FROM parser_location WHERE city_permalink = '{$permalink}') AS location_tmp
								INNER JOIN parser_station ON parser_station.place_id = location_tmp.place_id WHERE parser_station.status = 'ok') AS tmp0
					) AS tmp1
				) AS plug_tmp
			{$sql_join}
			GROUP BY name_permalink";

	/*
	$stat_counter = "SELECT
				place_id,
				name_permalink,
				plug_amount_total AS plug_amt,
				plug_id_1,
				p1.plug_value AS name_1,
				p1.plug_amount AS amt_1,
				plug_id_2,
				p2.plug_value AS name_2,
				p2.plug_amount AS amt_2,
				plug_id_3,
				p3.plug_value AS name_3,
				p3.plug_amount AS amt_3,
				plug_id_4,
				p4.plug_value AS name_4,
				p4.plug_amount AS amt_4,
				plug_id_5,
				p5.plug_value AS name_5,
				p5.plug_amount AS amt_5,
				plug_id_6,
				p6.plug_value AS name_6,
				p6.plug_amount AS amt_6,
				plug_id_7,
				p7.plug_value AS name_7,
				p7.plug_amount AS amt_7
			FROM
				(
					SELECT
						place_id,
						plug_amount_total,
						name_permalink,
						if (LOCATE(';}', plug_id_1) > 0, NULL, plug_id_1) AS plug_id_1,
						if (LOCATE(';}', plug_id_2) > 0, NULL, plug_id_2) AS plug_id_2,
						if (LOCATE(';}', plug_id_3) > 0, NULL, plug_id_3) AS plug_id_3,
						if (LOCATE(';}', plug_id_4) > 0, NULL, plug_id_4) AS plug_id_4,
						if (LOCATE(';}', plug_id_5) > 0, NULL, plug_id_5) AS plug_id_5,
						if (LOCATE(';}', plug_id_6) > 0, NULL, plug_id_6) AS plug_id_6,
						if (LOCATE(';}', plug_id_7) > 0, NULL, plug_id_7) AS plug_id_7
					FROM (
						SELECT
							place_id,
							name_permalink,
							plug_amount_total,
							TRIM(BOTH '\"' FROM plug_id_1) AS plug_id_1,
							TRIM(BOTH '\"' FROM plug_id_2) AS plug_id_2,
							TRIM(BOTH '\"' FROM plug_id_3) AS plug_id_3,
							TRIM(BOTH '\"' FROM plug_id_4) AS plug_id_4,
							TRIM(BOTH '\"' FROM plug_id_5) AS plug_id_5,
							TRIM(BOTH '\"' FROM plug_id_6) AS plug_id_6,
							TRIM(BOTH '\"' FROM plug_id_7) AS plug_id_7
							FROM
								(SELECT
									location_tmp.place_id,
									location_tmp.name_permalink,
									parser_station.plug_amount AS plug_amount_total,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',2), ':', -1) AS plug_id_1,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',4), ':', -1) AS plug_id_2,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',6), ':', -1) AS plug_id_3,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',8), ':', -1) AS plug_id_4,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',10), ':', -1) AS plug_id_5,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',12), ':', -1) AS plug_id_6,
									SUBSTRING_INDEX(SUBSTRING_INDEX(plugs,';',14), ':', -1) AS plug_id_7
								FROM
									(SELECT place_id,name_permalink FROM parser_location WHERE city_permalink = '{$permalink}') AS location_tmp
								INNER JOIN parser_station ON parser_station.place_id = location_tmp.place_id) AS tmp0
					) AS tmp1
				) AS plug_tmp
			LEFT JOIN parser_plug AS p1 ON p1.plug_id = plug_tmp.plug_id_1
			LEFT JOIN parser_plug AS p2 ON p2.plug_id = plug_tmp.plug_id_2
			LEFT JOIN parser_plug AS p3 ON p3.plug_id = plug_tmp.plug_id_3
			LEFT JOIN parser_plug AS p4 ON p4.plug_id = plug_tmp.plug_id_4
			LEFT JOIN parser_plug AS p5 ON p5.plug_id = plug_tmp.plug_id_5
			LEFT JOIN parser_plug AS p6 ON p6.plug_id = plug_tmp.plug_id_6
			LEFT JOIN parser_plug AS p7 ON p7.plug_id = plug_tmp.plug_id_7
			GROUP BY name_permalink";
		*/
		$stats = array();
		$cache_key ='query-plugs-1-'.$permalink;
		if ($permalink != '' && $target == '2') {

			if ( ! $html = get_transient( $cache_key ) ) {
				//fresh query & view cache generation for next 24hrs
				$expiration = 60*60*24;
				$stats = $wpdb->get_results($stat_counter);
				$plug_amt = $typ2 = $schuko = $combined = $chademo = $tesla = 0;
				if (!empty($stats)) {
					foreach ($stats as $stat) {
						$tmp = (array) $stat;
						$plug_amt += $stat->plug_amt;
						$tmps = array_values($tmp);
						foreach ($tmps as $k => $val) {
							switch($val){
								//typ2
								case (strpos($val, 'Typ 2 ') !== false) :
									$typ2 += $tmps[$k + 1];
									break;
								//schuko
								case (strpos($val, 'Schuko') !== false) :
									$schuko += $tmps[$k + 1];
									break;
								//combined
								case (strpos($val, 'Combined') !== false) :
									$combined += $tmps[$k + 1];
									break;
								//chademo
								case (strpos($val, 'CHAdeMO') !== false) :
									$chademo += $tmps[$k + 1];
									break;
								//tesla
								case (strpos($val, 'Tesla Supercharger') !== false) :
									$tesla += $tmps[$k + 1];
									break;
								default:
									break;
							}
						}
					}
				}
				ob_start();
				?>
				<div class="box box-shadow city-summary">
						<h1>Alle Ladestationen <?php echo ($permalink == '' || $target == '3') ? '' : 'in';?> <?php echo $headline; ?></h1>
						<p>Das Ladesäulen-Netz wächst auch in <?php echo $headline; ?> mit schnellen Schritten. Für das Stadtgebiet haben wir derzeit <?php echo count($stats); ?> Standorte mit <?php echo $plug_amt; ?> Ladepunkten in unserer Ladesäulen-Datenbank. Davon sind <?php echo $typ2; ?> Ladepunkte vom Typ 2 (bis zu 22 kw), <?php echo $schuko; ?> Schuko (bis zu 5 kw),  <?php echo $combined; ?> Combined Charging (bis zu 50 kw) <?php echo $chademo; ?> CHAdeMO  und <?php echo $tesla; ?> Tesla-Schnellladesäulen. Der Zugang funktioniert meist über eine <a href="<?php echo get_home_url(); ?>/elektromobilitat-ueberblick-ueber-ladetarife-und-bezahlsysteme/">Ladekarte</a> oder das Smartphone.</p>
						<br>
						<p>Wenn Sie weitere Ladesäulen kennen, die sich noch nicht in unserer Liste befinden, geben Sie uns bitte eine <a href="<?php echo get_home_url(); ?>/contact/">Nachricht</a>.</p>
					</div>
				<?php
				$html = ob_get_clean();
				set_transient( $cache_key, $html, $expiration );
			}

		}
?>
<section class="ct-box"></section>
<?php if ($permalink != '') : ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo KEY; ?>&callback=initMapMarker" async defer></script>
<script>
	var map;
    function initMapMarker() {
    	var locations = <?php echo json_encode($markers); ?>;
    	map = new google.maps.Map(document.getElementById('mapcity'), {
	        zoom: 12,
	        mapTypeControl: false,
	        scale: 2,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_CENTER
			}
        });
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
        var image = jQuery('#map_layer').attr('data-url') + '/wp-content/themes/mein-e-fahrzeug/img/marker-sm.png';


	    var marker, i;

	    for (i = 0; i < locations.length; i++) {
	    	marker = new google.maps.Marker({
	    		position: new google.maps.LatLng(parseFloat(locations[i][1]), parseFloat(locations[i][2])),
	        	map: map,
	        	icon: image,
	        	animation: google.maps.Animation.DROP
	      	});

	      	bounds.extend(marker.position);

	    	google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    		var ct = '<b><a href="/stromtankstellen/'+locations[i][7]+'/">' + locations[i][0] + '</a></b>' + locations[i][4] + '<br>'+ locations[i][5];
	        	return function() {
	        		infowindow.setContent(ct);
	          		infowindow.open(map, marker);
	        	}
	      	})(marker, i));
	    }
	    map.fitBounds(bounds);
    }
</script>
<section class="ct-map-wrapper ct-city-map-wrapper">
	<div class="container" style="position: relative;">
		<?php if (count($markers) > 0) :?>
		<div id="map_layer" class="map-layer" data-url="<?php echo get_home_url(); ?>">
			<div id="mapcity"></div>
		</div>
		<?php endif; ?>
		<div class="breadcrumb-nav">
			<div class="nav">
				<ol class="list-unstyled e-station-detail-list" itemscope="" itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>">
							<i class="fa fa-home fa-2x"></i>
							<span class="hide" itemprop="name">Home</span>
						</a>
						<meta itemprop="position" content="1">
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/stromtankstellen/">
							<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name">Stromtankstellen Liste</span></strong>
						</a>
						<meta itemprop="position" content="2">
					</li>
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink; ?>/">
							<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $headline; ?></span></strong>
						</a>
						<meta itemprop="position" content="3">
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<section class="ct-stromtankstellen e-station-detail">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<?php if ($permalink != '' && $target == '2') : ?>
				<?php echo $html; ?>
				<?php endif; ?>
				<div class="box box-shadow station-list" id="station_list" data-target="<?php echo ($is_country) ? 1 : 2; ?>" data-permalink="<?php echo $permalink; ?>">
					<h1>Übersicht über Ladesäulen</h1>
					<?php if (count($locations) > 0) :?>
						<ul class="list-unstyled e-station-list">
							<?php foreach ($locations as $loc) :?>
								<?php
									$urlencode = implode('/', array_map('rawurlencode', explode('/', $loc->name_permalink)));
									$urlencode = implode('+', array_map('rawurlencode', explode('+', $loc->name_permalink)));
								?>
							<li><a href="<?php echo get_home_url(); ?>/<?php echo $link_base; ?>/<?php echo $urlencode;?>/"><?php echo $loc->name; ?></a></li>
							<?php endforeach; ?>
						</ul>

						<div class="text-center e-station-list-page">
							<ul class="list-unstyled pagination">
								<?php if ($page_num > 1) :?>
								<li class="first"><a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink_url; ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
								<li class="prev"><a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink_url; ?><?php echo ($page_num - 1) > 1 ? 'seite-'.($page_num - 1).'/' : '';?>" ><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
								<?php endif; ?>
								<?php

								// list display
								foreach ($pages_ellipsis as $page) {
								    // If page has a link
								    if (isset ($page['url'])) { ?>
								        <li><a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink_url; ?><?php echo ($page['text'] > 1) ? 'seite-'.$page['text'].'/' : '';?>" >
										<?php echo $page['text'] ?>
									</a></li>
								<?php }
								    // no link - just display the text
								     else
								     	if ($page['text'] == $page_num) {
								     		echo '<li class="active"><a href="#">'.$page['text'].'</a></li>';
								     	} else {
								     		echo '<li><a href="#" class="ellipsis">'.$page['text'].'</a></li>';
								     	}
								} ?>
								<?php if ($page_num < $max) : ?>
									<li class="next"><a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink_url; ?>seite-<?php echo ($page_num + 1); ?>/"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									<li class="last"><a href="<?php echo get_home_url(); ?>/stromtankstellen-list/<?php echo $permalink_url; ?>seite-<?php echo $max; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-3">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
