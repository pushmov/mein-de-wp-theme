<?php
	global $wpdb;

	$page_num = isset($_POST['pagenum']) ? $_POST['pagenum'] : 1;
	$offset = ($page_num == 1) ? 0 : 10 * ($page_num - 1);

	/**
	action	get_vehicle_overview
	car_maker	
	pagenum	1
	price	descending
	range	
	type	electric
	*/

	$params = $_POST;

	
	$where = "";
	if (($params['car_maker'] != '' && !in_array('alle', $params['car_maker'])) || $params['type'] != '') {
		$where = " WHERE ";
	}

	if ($params['car_maker'] != '' && !in_array('alle', $params['car_maker'])) {
		$makers = implode("','", $params['car_maker']);
		$where .= " car_maker IN ('".$makers."')";
	}

	if ($params['type'] != '') {
		if ($params['car_maker'] != '') {
			$where .= " AND";
		}
		if ($params['type'] == 'electric') {
			$like = '%Elektrische Auto%';
		} else {
			$like = 'Plug-in Hybride%';
		}
		$where .= " car_kind LIKE '{$like}'";
	}

	//order
	$order_param = array();
	if ($params['price'] == 'ascending') {
		$order_param[] = "if(car_price = '' or car_price is null,1,0),CAST(car_price AS unsigned) ASC";
	}

	if ($params['price'] == 'descending') {
		$order_param[] = "if(car_price = '' or car_price is null,1,0),CAST(car_price AS unsigned) DESC";
	}

	if ($params['range'] == 'ascending') {
		$order_param[] = "if(electric_range = '' or electric_range is null,1,0),CAST(electric_range AS unsigned) ASC";
	}

	if ($params['range'] == 'descending') {
		$order_param[] = "if(electric_range = '' or electric_range is null,1,0),CAST(electric_range AS unsigned) DESC";
	}
	$order = ( $params['price'] == '' && $params['range'] == '' ) ? " ORDER BY vehicle_id ASC" : " ORDER BY ".join(',', $order_param);

	$total_vehicles = $wpdb->get_results( "SELECT COUNT(*) AS total FROM parser_evdatabase_vehicles " . $where );
	$pages = ceil($total_vehicles[0]->total / 10);
	$myrows = $wpdb->get_results( "SELECT * FROM parser_evdatabase_vehicles " . $where . " " . $order . " LIMIT {$offset},10" );
	

?>
<div class="box box-shadow box-overview">
	<?php if (!empty($myrows)) :?>
	<?php foreach ($myrows as $row) :?>
	<div class="car-info clearfix">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<?php 
					$imgs = unserialize($row->image_url);
					$cdn_url = 'https://cdn.mein-e-fahrzeug.de/';
					$cdn_name = $cdn_url . basename($imgs[0]);
				?>
				<img src="<?php echo get_template_directory_uri() . '/img/no_image.jpg'	?>" data-src="<?php echo $cdn_name;	?>" class="img-responsive box-shadow lazy" alt="<?php echo $row->car_name?>" title="<?php echo $row->car_name?>">
			</div>
			<div class="col-xs-12 col-sm-8">
				<?php $delimiter = '-';
					//$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $row->car_name))))), $delimiter));
				$slug = str_replace('-', '--', strtolower($row->car_name));
				$slug = str_replace(' ', '-', $slug);
				?>
				<h3><a href="<?php echo get_home_url(); ?>/elektroauto/<?php echo $slug;?>/"><?php echo $row->car_name; ?></a></h3>
				<?php $kind = strstr($row->car_kind, 'Hybride') ? 'Plugin-Hybride' : 'Elektrisch'; ?>
				<strong><?php echo $kind; ?></strong>
				<div class="clearfix table">
					<div class="col-sm-4 border-bottom text-center">
						<div class="row">
							<div class="col-xs-6 col-sm-12 cell">Reichweite</div>
							<div class="col-xs-6 col-sm-12 cell-item"><?php echo ($row->electric_range == '') ? 'NA' : $row->electric_range.'km'; ?></div>
						</div>
					</div>
					<div class="col-sm-4 border-bottom text-center">
						<div class="row">
							<div class="col-xs-6 col-sm-12 cell"><span class="e-auto-item full">Höchstgeschwindigkeit</span><span class="e-auto-item break">Höchst-<br>geschwindigkeit</span></div>
							<div class="col-xs-6 col-sm-12 cell-item"><span class="e-auto-item full"><?php echo $row->top_speed; ?> km/h</span><span class="e-auto-item break"><br><?php echo $row->top_speed; ?> km/h</span></div>
						</div>
					</div>
					<div class="col-sm-4 border-bottom text-center">
						<div class="row">
							<div class="col-xs-6 col-sm-12 cell">Preis</div>
							<div class="col-xs-6 col-sm-12 cell-item">&euro; <?php echo number_format($row->car_price,0,",","."); ?>,-</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
	<?php endif; ?>
</div>

<script type="text/javascript">
	$('.lazy').each(function(i, v){
		var el = $(v);
		var downloadingImage = new Image();
		downloadingImage.onload = function(){
		    v.src = this.src;
		};
		downloadingImage.src = el.attr('data-src');
	});
</script>

	<div class="text-center vehicle-pagination">
		<ul class="list-unstyled pagination">
		<?php if ($page_num > 1) :?>
		<li class="first"><a href="#" class="page" data-page="1"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
		<li class="prev"><a href="#" class="page" data-page="<?php echo $page_num - 1;?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
		<?php endif; ?>
		<?php 
			$pages_ellipsis = paginate_mein("#", "", $pages, $page_num, $radius = 2);
			// list display
			foreach ($pages_ellipsis as $page) {
			    // If page has a link
			    if (isset ($page['url'])) { ?>
			        <li><a href="<?php echo $page['url']?>" class="page" data-page="<?php echo $page['text'] ?>">
					<?php echo $page['text'] ?>
				</a></li>
			<?php }
			    // no link - just display the text
			     else 
			     	if ($page['text'] == $page_num) {
			     		echo '<li class="active"><a href="#">'.$page['text'].'</a></li>';
			     	} else {
			     		echo '<li><a href="#" class="ellipsis">'.$page['text'].'</a></li>';
			     	}
			        
			} ?>
			<?php if ($page_num < $pages) : ?>
				<li class="next"><a href="#" class="page" data-page="<?php echo $page_num + 1; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li class="last"><a href="#" class="page" data-page="<?php echo $pages; ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
			<?php endif; ?>
		</ul>
	</div>

<?php exit();?>