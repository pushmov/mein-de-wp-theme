<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mein-e-fahrzeug
 */

get_header();
?>

	<section class="ct-box">
</section>
	<section class="ct-news-detail">
		<div class="container">
			
			<div class="row">
				<div class="col-md-9">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content-page-custom', get_post_type() );

						//the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.

						?>
						
						<?php
						

					endwhile; // End of the loop.
					?>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>

<?php

get_footer();
