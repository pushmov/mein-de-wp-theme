<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mein-e-fahrzeug
 */
global $post;
global $placex;
global $wp_query;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if (urlencode($wp_query->query['place']) != $placex->name_permalink) : ?>
  <link rel="canonical" href="<?php echo get_home_url() .'/stromtankstellen/'. $placex->name_permalink;?>/" />
  <?php else : ?>
  <link rel="canonical" href="<?php echo get_home_url() .'/stromtankstellen/'. $placex->name_permalink;?>/" />
  <?php endif; ?>

  <?php 
    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
      $content = get_the_content();
      // regex (fixed) replacing '<embed>' with '(embed )'
      $content = preg_replace("/<embed?[^>]+>/i", "(embed) ", $content);
      // remove all tags
      $content = wp_strip_all_tags($content);
  ?>
  <?php if (get_the_title() != 'Stromtankstellen Single') : ?>
  <meta property="og:title" content="<?php echo the_title();?>" />
  <?php endif; ?>
  <?php if (limit_text($content, 50) != '') : ?>
  <meta property="og:description" content="<?php echo limit_text($content, 50); ?>" />
  <?php endif; ?>
  <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
  <?php if ($img[0] != '') : ?>
  <meta property="og:image" content="<?php echo $img[0]; ?>" />
  <?php endif; ?>
  <?php endwhile; endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<nav class="navbar navbar-default">
        <div class="container column">
        	<div class="navbar-header">
            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              		<span class="sr-only">Toggle navigation</span>
              		<span class="icon-bar"></span>
              		<span class="icon-bar"></span>
              		<span class="icon-bar"></span>
            	</button>
            	<a class="navbar-brand logo" href="<?php echo get_home_url(); ?>/"><img alt="mein-e-fahrzeuge.de" src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>
          	</div>
          	<div id="navbar" class="navbar-collapse collapse">
              <?php
              $menu_locations = wp_get_nav_menu_items('main_menu');
              ?>
              <?php if (!empty($menu_locations)) :?>
                <ul class="nav navbar-nav navbar-right">
                  <?php foreach ($menu_locations as $menu) :?>
                    <li><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></li>
                  <?php endforeach;?>
                </ul>
              <?php endif; ?>
          	</div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
	</nav>