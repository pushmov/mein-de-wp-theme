<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mein-e-fahrzeug
 */

?>
	<footer>
		<div class="container">

			<?php
              $menu_locations = wp_get_nav_menu_items('footer');
              ?>
              <?php if (!empty($menu_locations)) :?>
                <ul class="list-unstyled">
                  <?php foreach ($menu_locations as $menu) :?>
                    <li><a href="<?php echo $menu->url; ?>"><i class="fa fa-caret-right"></i><?php echo $menu->title; ?></a></li>
                  <?php endforeach;?>
                </ul>
              <?php endif; ?>
			<p>&copy; 2014 - 2018 www.mein-e-fahrzeug.de</p>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
