<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package mein-e-fahrzeug
 */

get_header();
?>
<section class="ct-box"></section>
<section class="ct-map-wrapper">
	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<section class="error-404 not-found">
					<div class="box box-shadow">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'mein-e-fahrzeug' ); ?></h1>
						<div class="page-content">
							<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'mein-e-fahrzeug' ); ?></p>

						</div><!-- .page-content -->
					</div>
				</section><!-- .error-404 -->

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
</section>

<?php
get_footer();
