<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mein-e-fahrzeug
 */
global $post;

$categories = get_the_category();
$cat = $categories[0];
get_header();
?>
<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  
	<section class="ct-box">
</section>
	<section class="ct-news-detail">
		<div class="container">
			<div class="breadcrumb-nav">

				<div class="clearfix">
					<div class="col-md-12">
						<div class="nav nav-vehicle">
							<ol class="list-unstyled e-station-detail-list" itemscope itemtype="http://schema.org/BreadcrumbList">
								<li itemprop="itemListElement" itemscope
									itemtype="http://schema.org/ListItem">
									<a itemscope itemtype="http://schema.org/Thing"
										itemprop="item" href="<?php echo get_home_url(); ?>/">
										<i class="fa fa-home fa-2x"></i>
										<span class="hide" itemprop="name">Home</span>
									</a>
									<meta itemprop="position" content="1" />
								</li>
								<li itemprop="itemListElement" itemscope 
									itemtype="http://schema.org/ListItem">
									<a itemscope itemtype="http://schema.org/Thing" 
										itemprop="item" href="<?php echo get_home_url(); ?>/<?php echo $cat->slug; ?>/">
										<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $cat->name; ?></span></strong>
									</a>
									<meta itemprop="position" content="2" />
								</li>
								<li itemprop="itemListElement" itemscope
									itemtype="http://schema.org/ListItem">
									<a itemscope itemtype="http://schema.org/Thing"
										itemprop="item" href="<?php echo get_home_url(); ?>/<?php echo $post->post_name?>/">
										<i class="fa fa-caret-right fa-lg"></i><strong><span itemprop="name"><?php echo $post->post_title; ?></span></strong>
									</a>
									<meta itemprop="position" content="3" />
								</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content-news-detail', get_post_type() );

						//the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.

						?>
						<div class="box box-shadow post-single">
							<div class="comment-list">
								<?php
									$comment_args = apply_filters( 'comments_template_query_args', array(
										'post_id' => get_the_ID()
									) );
									$comment_query = new WP_Comment_Query;
									$comments = $comment_query->query($comment_args);
									if ($comments) {
										foreach ($comments as $comment) :
											$rating_meta = get_comment_meta($comment->comment_ID, 'rating');
											$rating = isset($rating_meta[0]) ? $rating_meta[0] : 0;

											$context_meta = get_comment_meta($comment->comment_ID, 'context');
											$context_id = get_comment_meta($comment->comment_ID, 'context_id');

											$width = $rating * 20;
							
							?>
							<div class="comment-item" id="comment-<?php echo $comment->comment_ID; ?>">
								<div class="comment-meta post-meta" role="complementary">

									<?php if ($comment->comment_approved != '0') : ?>
									<div class="row">
										
										<div class="col-sm-12">
											<?php
												$month = array(
													'01' => 'Januar',
													'02' => 'Februar',
													'03' => 'März',
													'04' => 'April',
													'05' => 'Mai',
													'06' => 'Juni',
													'07' => 'Juli',
													'08' => 'August',
													'09' => 'September',
													'10' => 'Oktober',
													'11' => 'November',
													'12' => 'Dezember'
												);
												$gdate[] = date('d.', strtotime($comment->comment_date));
												$gdate[] = $month[date('m', strtotime($comment->comment_date))];
												$gdate[] = date('Y', strtotime($comment->comment_date));

												$gtime[] = date('H:i', strtotime($comment->comment_date));
												$gtime[] = 'Uhr';
											?>
											<h2 class="comment-author"><em><?php comment_author(); ?> am 
												<time class="comment-meta-item" datetime="<?php comment_date('Y-m-d') ?>T<?php comment_time('H:iP') ?>" itemprop="datePublished"><?php echo join(' ', $gdate); ?>, <?php echo join(' ', $gtime); ?></time>
											</em></h2>
										</div>
									</div>
									<?php endif; ?>
									
									<?php //edit_comment_link('<p class="comment-meta-item">Edit this comment</p>','',''); ?>
									<?php if ($comment->comment_approved == '0') : ?>
									<p class="comment-meta-item">Your comment is awaiting moderation.</p>
									<?php endif; ?>
								</div>
								<div class="comment-content post-content" itemprop="text">
									<?php if ($comment->comment_approved != '0') : ?>
									<?php comment_text() ?>
									<?php endif; ?>
								</div>
							</div>
							<?php endforeach; } ?>
							</div>
							<?php comments_template(); ?>
						</div>
						<?php
						

					endwhile; // End of the loop.
					?>
				</div>
				<div class="col-md-3">
					<?php get_sidebar('news'); ?>
				</div>
			</div>
		</div>
	</section>
	

<?php

get_footer();
?>
<script type="text/javascript">
		if (jQuery('#respond.comment-respond').length <= 0) {
			jQuery('.box.box-shadow.post-single').addClass('hide');
		}
	</script>