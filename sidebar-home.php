<?php
	$widgets = array();
	$widget_content = '';
	$sidebar_id = 'sidebar-3';
	$sidebars_widgets = wp_get_sidebars_widgets();
	$widget_ids = $sidebars_widgets[$sidebar_id]; 

	foreach( $widget_ids as $id ) {
		$wdgtvar = 'widget_'._get_widget_id_base( $id );
		$idvar = _get_widget_id_base( $id );
		$instance = get_option( $wdgtvar );
		$idbs = str_replace( $idvar.'-', '', $id );
		$widgets[$instance[$idbs]['title']] = $instance[$idbs]['content'];
	}
?>

<?php if ($widget_content != '' ) : ?>
<h3><i class="fa fa-caret-right"></i>Beispieltext über E-Autos</h3>
<div class="box box-shadow">
	<?php echo $widget_content; ?>
</div>
<?php endif; ?>

<ul class="list-unstyled social-media">
	<?php if ($widgets['Facebook'] != '') : ?>
	<li><a href="<?php echo $widgets['Facebook'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" /></a></li>
	<?php endif; ?>

	<?php if ($widgets['Twitter'] != '') : ?>
	<li><a href="<?php echo $widgets['Twitter'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" /></a></li>
	<?php endif; ?>
</ul>
<div class="adsense-front">
	<?php if ($widgets['Adsense'] != '') :?>
		<?php echo $widgets['Adsense']; ?>
	<?php endif; ?>
</div>